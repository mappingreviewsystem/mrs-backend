source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.2.0'

# Use sqlite3 as the database for Active Record
gem 'pg', '~> 1'
# gem 'sqlite3'
# Use Puma as the app server
gem 'puma', '~> 3.7'
# Use SCSS for stylesheets
# gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
# gem 'uglifier', '>= 1.3.0'
# Transpile app-like JavaScript. Read more: https://github.com/rails/webpacker
# gem 'webpacker'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use CoffeeScript for .coffee assets and views
# gem 'coffee-rails', '~> 4.2'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
# gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 3.0'
# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.12'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.1.0', require: false

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '~> 2.13'
  gem 'selenium-webdriver'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  # gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  # gem 'spring'
  # gem 'spring-watcher-listen', '~> 2.0.0'

  # debug enviroment
  # gem 'better_errors'
  gem 'binding_of_caller'

  # verifies n+1 queries in db
  gem 'bullet'

  # generate entity-relation diagram
  gem 'rails-erd', require: false

  # find missing migration indexes
  gem 'lol_dba'

  # pry console
  gem 'pry-byebug'
  gem 'pry-rails'

  # verify missing unique indexes
  gem 'consistency_fail', require: false
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

# file upload
gem 'paperclip'

# ActiveRecord mass import
gem 'activerecord-import'

# background jobs
gem 'sidekiq'
gem 'sidekiq-limit_fetch'
gem 'sidekiq-status'
# gem 'sidekiq-unique-jobs'

# web crawler (to download articles without direct link)
gem 'mechanize'

# frontend framework
# gem 'semantic-ui-sass'
# gem 'jquery-rails'

# icons
# gem 'font-awesome-sass'

# user auth
gem 'devise'

# user permissions
gem 'pundit'

# run rails and webpack in same command
gem 'foreman'

# DRY responses in controllers
gem 'responders'

# edit forms in place
# gem 'best_in_place', '~> 3.0.1'

# read pdf documents (to extract text from them)
# gem 'pdf-reader' # this gem don't handle multiple text columns
gem 'docsplit'

# export/import db via .csv
# gem 'datashift'
# gem 'erubis'

# step-by-step wizard controllers
gem 'wicked'

# drag and drop ajax file upload
# gem 'dropzonejs-rails'

# pagination
gem 'kaminari'

# full-text search (ElasticSearch)
# TODO: compare with Chewy [https://github.com/toptal/chewy]
gem 'searchkick'

# dump db to seeds
gem 'seed_dump'

# jsonapi-rb
gem 'active_model_serializers', '~> 0.10.0'
gem 'oj'

# extract identifiers from articles' pages
# gem 'identifiers', '~> 0.9'

# retrieve article's data
gem 'library_stdnums'
gem 'serrano'

# strip attributes
gem 'auto_strip_attributes', '~> 2.2'

# integrate rails cli with ember
# gem 'ember-cli-rails'

gem 'devise_token_auth'
gem 'omniauth'
# gem 'simple_token_authentication', '~> 1.0'
gem 'rack-cors', require: 'rack/cors'

# graph library
# gem 'rgl'

# GraphQL
gem 'graphql'

# automagically eager loading
gem 'goldiloader'

# clone models to fork researches
gem 'deep_cloneable', '~> 2.3.1'

# countries defaults
gem 'countries'

# transform blank record fields in nil
gem 'nilify_blanks'

# better counter cache
gem 'counter_culture', '~> 1.8'

# bibtex parser
gem 'bibtex-ruby'

# spreadsheet (.xls)
gem 'rubyzip', '>= 1.2.1'
gem 'axlsx', git: 'https://github.com/randym/axlsx.git', ref: 'c593a08'
gem 'axlsx_rails'
