class MigrateRecordsToReflectNewResearchReferences < ActiveRecord::Migration[5.1]
  def change
    Publication.all.find_each { |pub| pub.update(research_id: pub.articles.try(:first).try(:research_id)) }
    Author.all.find_each { |author| author.update(research_id: author.articles.try(:first).try(:research_id)) }
    Affiliation.all.find_each { |affiliation| affiliation.update(research_id: affiliation.authors.try(:first).try(:research_id)) }
    Funder.all.find_each { |funder| funder.update(research_id: funder.articles.try(:first).try(:research_id)) }
  end
end
