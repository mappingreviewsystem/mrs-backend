class AddIssnIsbnToCrossRefs < ActiveRecord::Migration[5.1]
  def change
    change_table :crossrefs do |t|
      t.text :issn
      t.text :isbn
    end
    add_index :crossrefs, :doi, unique: true, where: 'doi IS NOT NULL'
    add_index :crossrefs, :issn, unique: true, where: 'issn IS NOT NULL'
    add_index :crossrefs, :isbn, unique: true, where: 'isbn IS NOT NULL'
  end
end
