class CreateContentTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :content_types do |t|
      t.text :code, null: false

      t.timestamps
    end
    add_index :content_types, [:code], unique: true
  end
end
