class ChangeCriteriaMeetAttribute < ActiveRecord::Migration[5.1]
  def change
    remove_column :article_inclusion_criteria, :meet
    remove_column :article_exclusion_criteria, :meet
  end
end
