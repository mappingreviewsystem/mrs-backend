class AddResearchToFunders < ActiveRecord::Migration[5.1]
  def change
    change_table :funders do |t|
      t.references :research
    end
  end
end
