class DropTableArticlesAuthors < ActiveRecord::Migration[5.1]
  def change
    drop_table :articles_authors
  end
end
