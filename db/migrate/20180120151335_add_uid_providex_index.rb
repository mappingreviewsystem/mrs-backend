class AddUidProvidexIndex < ActiveRecord::Migration[5.1]
  def change
    remove_column :users, :authentication_token
    # remove_index :users, :authentication_token
    add_index :users, [:uid, :provider], unique: true
  end
end
