class RemoveArticleIndex < ActiveRecord::Migration[5.1]
  def change
    # TODO: it can have duplicate title if DOI is different, find a way to reflect it in postgres
    remove_index :articles, [:research_id, :title]
  end
end
