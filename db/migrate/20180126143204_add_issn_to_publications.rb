class AddIssnToPublications < ActiveRecord::Migration[5.1]
  def change
    change_table :publications do |t|
      t.text :issn
    end
  end
end
