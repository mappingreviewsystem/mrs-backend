class CreateNoteArticles < ActiveRecord::Migration[5.1]
  def change
    create_table :note_articles do |t|
      t.references :note, foreign_key: true
      t.references :article, foreign_key: true

      t.timestamps
    end
  end
end
