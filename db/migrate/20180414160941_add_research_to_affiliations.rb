class AddResearchToAffiliations < ActiveRecord::Migration[5.1]
  def change
    change_table :affiliations do |t|
      t.references :research
    end
    add_index :affiliations, [:name, :research_id], unique: true
  end
end
