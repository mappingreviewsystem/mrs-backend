class MigrateArticleDatabaseFromResearchArticleDatabase < ActiveRecord::Migration[5.1]
  def change
    change_table :article_databases do |t|
      # t.text "title"
      # t.text "url"
      t.text :query
      t.references :research
      t.attachment :exported_search
      t.boolean :downloaded, default: false, null: false
      t.text :missing_lines
      t.integer :status, default: 0, null: false
    end
    add_index :article_databases, [:title, :research_id], unique: true
  end
end
