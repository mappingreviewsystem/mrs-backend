class RemoveArticleReferencesFromNotes < ActiveRecord::Migration[5.1]
  def change
    remove_column :notes, :article_id
  end
end
