class AddAbstractToArticles < ActiveRecord::Migration[5.1]
  def change
    change_table :articles do |t|
      t.text :abstract
    end
  end
end
