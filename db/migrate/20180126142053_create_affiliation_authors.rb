class CreateAffiliationAuthors < ActiveRecord::Migration[5.1]
  def change
    create_table :affiliation_authors do |t|
      t.references :affiliation, foreign_key: true
      t.references :author, foreign_key: true

      t.timestamps
    end
  end
end
