class CreateExtractionQuestionOptions < ActiveRecord::Migration[5.2]
  def change
    create_table :extraction_question_options do |t|
      t.references :extraction_question, foreign_key: true
      t.text :title, null: false

      t.timestamps
    end
  end
end
