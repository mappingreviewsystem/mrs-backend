class AddReferencesToNotes < ActiveRecord::Migration[5.1]
  def change
    change_table :notes do |t|
      t.references :article
      t.references :research
    end
  end
end
