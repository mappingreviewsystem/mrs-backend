class AddUniqueIndexToFunders < ActiveRecord::Migration[5.1]
  def change
    add_index :funders, :doi, unique: true, where: 'doi IS NOT NULL'
    add_index :funders, :crossref_id, unique: true, where: 'crossref_id IS NOT NULL'
  end
end
