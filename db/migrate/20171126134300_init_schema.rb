class InitSchema < ActiveRecord::Migration[5.1]
  def up
    create_table "article_databases", force: :cascade do |t|
      t.string "title"
      t.string "url"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end
    create_table "article_exclusion_criteria", force: :cascade do |t|
      t.integer "article_id"
      t.integer "exclusion_criterium_id"
      t.boolean "meet", default: false, null: false
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.integer "doubt", default: 0
      t.index ["article_id"], name: "index_article_exclusion_criteria_on_article_id"
      t.index ["exclusion_criterium_id"], name: "index_article_exclusion_criteria_on_exclusion_criterium_id"
    end
    create_table "article_extraction_questions", force: :cascade do |t|
      t.integer "article_id"
      t.integer "extraction_question_id"
      t.text "value", default: "", null: false
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.integer "doubt", default: 0
      t.index ["article_id"], name: "index_article_extraction_questions_on_article_id"
      t.index ["extraction_question_id"], name: "index_article_extraction_questions_on_extraction_question_id"
    end
    create_table "article_inclusion_criteria", force: :cascade do |t|
      t.integer "article_id"
      t.integer "inclusion_criterium_id"
      t.boolean "meet", default: false, null: false
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.integer "doubt", default: 0
      t.index ["article_id"], name: "index_article_inclusion_criteria_on_article_id"
      t.index ["inclusion_criterium_id"], name: "index_article_inclusion_criteria_on_inclusion_criterium_id"
    end
    create_table "article_pages", force: :cascade do |t|
      t.integer "article_id", null: false
      t.text "value"
      t.integer "page", null: false
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.index ["article_id", "page"], name: "index_article_pages_on_article_id_and_page", unique: true
      t.index ["article_id"], name: "index_article_pages_on_article_id"
    end
    create_table "article_publications", force: :cascade do |t|
      t.integer "article_id"
      t.integer "publication_id"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.index ["article_id"], name: "index_article_publications_on_article_id"
      t.index ["publication_id"], name: "index_article_publications_on_publication_id"
    end
    create_table "articles", force: :cascade do |t|
      t.string "title"
      t.string "content_type"
      t.string "url"
      t.integer "publication_year"
      t.string "doi"
      t.string "volume"
      t.string "issue"
      t.string "page_start"
      t.string "page_end"
      t.string "isbn"
      t.string "issn"
      t.integer "status", default: 0
      t.string "data"
      t.integer "article_database_id"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.string "file_file_name"
      t.string "file_content_type"
      t.integer "file_file_size"
      t.datetime "file_updated_at"
      t.integer "research_id"
      t.index ["article_database_id"], name: "index_articles_on_article_database_id"
    end
    create_table "articles_authors", id: false, force: :cascade do |t|
      t.integer "article_id", null: false
      t.integer "author_id", null: false
    end
    create_table "articles_keywords", id: false, force: :cascade do |t|
      t.integer "article_id", null: false
      t.integer "keyword_id", null: false
    end
    create_table "authors", force: :cascade do |t|
      t.string "name"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end
    create_table "exclusion_criteria", force: :cascade do |t|
      t.string "title"
      t.integer "research_id"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.index ["research_id"], name: "index_exclusion_criteria_on_research_id"
    end
    create_table "extraction_questions", force: :cascade do |t|
      t.string "key"
      t.string "value"
      t.integer "research_id"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.integer "field_type", default: 0
      t.index ["research_id"], name: "index_extraction_questions_on_research_id"
    end
    create_table "extraction_questions_research_questions", id: false, force: :cascade do |t|
      t.integer "extraction_question_id", null: false
      t.integer "research_question_id", null: false
    end
    create_table "group_memberships", force: :cascade do |t|
      t.integer "group_id"
      t.integer "user_id"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.boolean "owner", default: false, null: false
      t.index ["group_id"], name: "index_group_memberships_on_group_id"
      t.index ["user_id"], name: "index_group_memberships_on_user_id"
    end
    create_table "groups", force: :cascade do |t|
      t.string "name"
      t.text "description"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end
    create_table "inclusion_criteria", force: :cascade do |t|
      t.string "title"
      t.integer "research_id"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.index ["research_id"], name: "index_inclusion_criteria_on_research_id"
    end
    create_table "keywords", force: :cascade do |t|
      t.string "title"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end
    create_table "keywords_researches", id: false, force: :cascade do |t|
      t.integer "keyword_id", null: false
      t.integer "research_id", null: false
    end
    create_table "publications", force: :cascade do |t|
      t.string "title"
      t.integer "publisher_id"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.index ["publisher_id"], name: "index_publications_on_publisher_id"
    end
    create_table "publishers", force: :cascade do |t|
      t.string "name"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end
    create_table "research_article_databases", force: :cascade do |t|
      t.string "query"
      t.integer "research_id"
      t.integer "article_database_id"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.string "exported_search_file_name"
      t.string "exported_search_content_type"
      t.integer "exported_search_file_size"
      t.datetime "exported_search_updated_at"
      t.boolean "downloaded", default: false, null: false
      t.text "missing_lines"
      t.integer "status", default: 0, null: false
      t.index ["article_database_id"], name: "index_research_article_databases_on_article_database_id"
      t.index ["research_id"], name: "index_research_article_databases_on_research_id"
    end
    create_table "research_questions", force: :cascade do |t|
      t.string "title"
      t.string "description"
      t.string "related_metadata"
      t.integer "research_id"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.index ["research_id"], name: "index_research_questions_on_research_id"
    end
    create_table "research_research_questions", force: :cascade do |t|
      t.integer "research_id"
      t.integer "research_question_id"
      t.string "value"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.index ["research_id"], name: "index_research_research_questions_on_research_id"
      t.index ["research_question_id"], name: "index_research_research_questions_on_research_question_id"
    end
    create_table "researches", force: :cascade do |t|
      t.string "title"
      t.string "objective"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.integer "user_id"
      t.integer "group_id"
    end
    create_table "users", force: :cascade do |t|
      t.string "email", default: "", null: false
      t.string "encrypted_password", default: "", null: false
      t.string "reset_password_token"
      t.datetime "reset_password_sent_at"
      t.datetime "remember_created_at"
      t.integer "sign_in_count", default: 0, null: false
      t.datetime "current_sign_in_at"
      t.datetime "last_sign_in_at"
      t.string "current_sign_in_ip"
      t.string "last_sign_in_ip"
      t.integer "role", default: 0, null: false
      t.integer "current_research"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.string "name"
      t.index ["email"], name: "index_users_on_email", unique: true
      t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    end
  end

  def down
    raise ActiveRecord::IrreversibleMigration, "The initial migration is not revertable"
  end
end
