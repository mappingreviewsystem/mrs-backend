class DropTableResearchesKeywords < ActiveRecord::Migration[5.1]
  def change
    drop_table :keywords_researches
  end
end
