class CreateExtractionQuestionOptionArticleExtractionQuestions < ActiveRecord::Migration[5.2]
  def change
    create_table :extraction_question_option_article_extraction_questions do |t|
      t.references :extraction_question_option, foreign_key: true, index: { name: 'index_e_q_option_on_e_q_option_a_e_q_id' }
      t.references :article_extraction_question, foreign_key: true, index: { name: 'index_a_e_q_on_e_q_option_a_e_q_id' }

      t.timestamps
    end
  end
end
