class AddUniqueIndexToArticles < ActiveRecord::Migration[5.1]
  def change
    add_index :articles, [:title, :doi, :research_id], unique: true, where: 'doi IS NOT NULL'
  end
end
