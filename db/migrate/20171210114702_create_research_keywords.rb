class CreateResearchKeywords < ActiveRecord::Migration[5.1]
  def change
    create_table :research_keywords do |t|
      t.references :research, index: true, foreign_key: true
      t.references :keyword, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
