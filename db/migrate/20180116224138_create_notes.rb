class CreateNotes < ActiveRecord::Migration[5.1]
  def change
    create_table :notes do |t|
      t.references :article, foreign_key: true
      t.text :value, null: false, default: ''
      t.string :color, null: false, default: 'red', limit: 6
      t.integer :priority, default: 0

      t.timestamps
    end
  end
end
