class CreateNoteResearches < ActiveRecord::Migration[5.1]
  def change
    create_table :note_researches do |t|
      t.references :note, foreign_key: true
      t.references :research, foreign_key: true

      t.timestamps
    end
  end
end
