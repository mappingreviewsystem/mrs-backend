class MigrateResearchArticleDatabaseToArticleDatabase < ActiveRecord::Migration[5.1]
  def change
    removal_ids = ArticleDatabase.all.pluck(:id)

    # migrate existing research_article_databases
    ResearchArticleDatabase.all.find_each do |rad|
      att = rad.attributes
      att.delete('id')
      att.delete('article_database_id')
      old = rad.article_database
      att['title'] = old.title
      att['url'] = old.url
      ArticleDatabase.create(att.compact.symbolize_keys)
    end

    # migrate existing research_article_databases
    Article.all.find_each do |a|
      next if a.article_database.nil?

      old = a.article_database
      new = ArticleDatabase.find_by(title: old.title, research_id: a.research_id)
      a.update(article_database_id: new.id)
    end

    # remove old ones
    ArticleDatabase.where(id: removal_ids).destroy_all
  end
end
