class AddDoiToAuthors < ActiveRecord::Migration[5.1]
  def change
    change_table :authors do |t|
      t.text :doi
    end
  end
end
