class AddResearchToPublications < ActiveRecord::Migration[5.1]
  def change
    change_table :publications do |t|
      t.references :research
    end
    add_index :publications, [:issn, :research_id], unique: true, where: 'issn IS NOT NULL'
    add_index :publications, [:isbn, :research_id], unique: true, where: 'isbn IS NOT NULL'
  end
end
