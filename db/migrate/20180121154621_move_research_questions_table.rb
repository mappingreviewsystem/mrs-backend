class MoveResearchQuestionsTable < ActiveRecord::Migration[5.1]
  def change
    add_column :research_questions, :value, :text, null: false, default: ''
    ResearchResearchQuestion.find_each do |question|
      question.research_question.update(value: question.value)
    end
    drop_table :research_research_questions
  end
end
