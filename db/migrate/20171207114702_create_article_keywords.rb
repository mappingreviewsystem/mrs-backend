class CreateArticleKeywords < ActiveRecord::Migration[5.1]
  def change
    create_table :article_keywords do |t|
      t.references :article, index: true, foreign_key: true
      t.references :keyword, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
