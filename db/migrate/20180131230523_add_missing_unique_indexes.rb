class AddMissingUniqueIndexes < ActiveRecord::Migration[5.1]
  def change
    add_index :articles, [:research_id, :title], unique: true
    add_index :article_authors, [:article_id, :author_id], unique: true
    add_index :article_exclusion_criteria, [:article_id, :exclusion_criterium_id], unique: true, name: 'article_exclusion_criteria_unique_index'
    add_index :article_extraction_questions, [:article_id, :extraction_question_id], unique: true, name: 'article_extraction_questions_unique_index'
    add_index :article_inclusion_criteria, [:article_id, :inclusion_criterium_id], unique: true, name: 'article_inclusion_criteria_unique_index'
    add_index :article_keywords, [:article_id, :keyword_id], unique: true
    add_index :group_memberships, [:user_id, :group_id], unique: true
    add_index :publications, [:title, :publisher_id], unique: true
    add_index :research_article_databases, [:research_id, :article_database_id], unique: true, name: 'research_article_databases_unique_index'
    add_index :research_keywords, [:research_id, :keyword_id], unique: true
    remove_index :article_publications, [:article_id]
    add_index :article_publications, [:article_id], unique: true
    remove_index :note_articles, [:note_id]
    add_index :note_articles, [:note_id], unique: true
    remove_index :note_researches, [:note_id]
    add_index :note_researches, [:note_id], unique: true
  end
end
