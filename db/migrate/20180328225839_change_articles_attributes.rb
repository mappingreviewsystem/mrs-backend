class ChangeArticlesAttributes < ActiveRecord::Migration[5.1]
  def change
    remove_column :articles, :content_type, :text
    add_column :articles, :content_type_id, :integer, default: 1
  end
end
