class CreateCrossrefs < ActiveRecord::Migration[5.1]
  def change
    create_table :crossrefs do |t|
      t.text :doi
      t.text :data

      t.timestamps
    end
  end
end
