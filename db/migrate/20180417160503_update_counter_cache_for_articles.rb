class UpdateCounterCacheForArticles < ActiveRecord::Migration[5.1]
  def change
    ArticleReference.counter_culture_fix_counts
  end
end
