class CreateFunders < ActiveRecord::Migration[5.1]
  def change
    create_table :funders do |t|
      t.text :name
      t.text :url
      t.text :location
      t.text :crossref_id

      t.timestamps
    end
  end
end
