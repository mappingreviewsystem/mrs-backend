class AddLocationToAffiliations < ActiveRecord::Migration[5.1]
  def change
    change_table :affiliations do |t|
      t.text :location
    end
  end
end
