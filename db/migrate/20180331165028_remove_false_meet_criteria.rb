class RemoveFalseMeetCriteria < ActiveRecord::Migration[5.1]
  def change
    ArticleInclusionCriterium.where(meet: false).destroy_all
    ArticleExclusionCriterium.where(meet: false).destroy_all
  end
end
