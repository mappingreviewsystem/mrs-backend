class ChangeReferenceCounterForArticles < ActiveRecord::Migration[5.1]
  def change
    Article.where(references_count: nil).update_all(references_count: 0)
    Article.where(referenced_by_count: nil).update_all(referenced_by_count: 0)

    # Change the column to not allow null
    change_column :articles, :references_count, :integer, null: false, default: 0
    change_column :articles, :referenced_by_count, :integer, null: false, default: 0
  end
end
