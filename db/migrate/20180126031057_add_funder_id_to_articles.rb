class AddFunderIdToArticles < ActiveRecord::Migration[5.1]
  def change
    add_reference :articles, :funder, foreign_key: true
  end
end
