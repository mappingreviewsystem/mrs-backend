class ChangeExtractionQuestion < ActiveRecord::Migration[5.2]
  def change
    change_table :extraction_questions do |t|
      t.text :range_min
      t.text :range_max
      t.text :range_increment
    end
    change_column :extraction_questions, :field_type, :integer, default: 0, null: false
  end
end
