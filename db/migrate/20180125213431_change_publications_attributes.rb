class ChangePublicationsAttributes < ActiveRecord::Migration[5.1]
  def change
    change_table :publications do |t|
      t.text :publication_type, default: ''
      t.text :location, default: ''
    end
  end
end
