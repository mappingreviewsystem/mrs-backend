class AddResearchToAuthors < ActiveRecord::Migration[5.1]
  def change
    change_table :authors do |t|
      t.references :research
    end
  end
end
