class MigrateResearchesKeywords < ActiveRecord::Migration[5.1]
  def change
    Research.transaction do
      Research.find_each do |research|
        research.keywords = research.deprecated_keywords
      end
    end
  end
end
