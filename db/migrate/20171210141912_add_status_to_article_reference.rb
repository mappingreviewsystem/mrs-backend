class AddStatusToArticleReference < ActiveRecord::Migration[5.1]
  def change
    add_column :article_references, :status, :integer, null: false, default: 0
  end
end
