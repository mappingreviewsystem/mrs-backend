class ChangeStatusValueInArticles < ActiveRecord::Migration[5.1]
  def change
    # enum status: { extracted: 3, selected: 2, unseen: 0, excluded: 1 }
    change_column :articles, :status, "integer USING (CASE status WHEN 0 THEN 2 WHEN 1 THEN 3 WHEN 2 THEN 1 WHEN 3 THEN 0 END)", null: false, default: 2
  end
end
