class ChangeIndexToReflectArticlesLogic < ActiveRecord::Migration[5.1]
  def change
    remove_index :articles, [:title, :doi, :research_id]
    add_index :articles, [:doi, :research_id], unique: true, where: 'doi IS NOT NULL'
    add_index :articles, [:title, :research_id], unique: true, where: 'doi IS NULL'
  end
end
