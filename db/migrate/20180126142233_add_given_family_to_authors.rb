class AddGivenFamilyToAuthors < ActiveRecord::Migration[5.1]
  def change
    change_table :authors do |t|
      t.text :given
      t.text :family
    end
  end
end
