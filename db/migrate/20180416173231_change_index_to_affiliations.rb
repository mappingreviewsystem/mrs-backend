class ChangeIndexToAffiliations < ActiveRecord::Migration[5.1]
  def change
    remove_index :affiliations, [:name, :research_id]
    add_index :affiliations, [:name, :location, :research_id], unique: true
  end
end
