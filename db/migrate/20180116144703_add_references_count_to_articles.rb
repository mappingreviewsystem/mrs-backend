class AddReferencesCountToArticles < ActiveRecord::Migration[5.1]
  def change
    change_table :articles do |t|
      t.integer :references_count, default: 0
    end

    reversible do |dir|
      dir.up { data }
    end
  end

  def data
    execute <<-SQL.squish
        UPDATE articles
           SET references_count = (SELECT count(1)
                                   FROM article_references
                                  WHERE article_references.source_id = articles.id)
    SQL
  end
end
