class ChangeTags < ActiveRecord::Migration[5.1]
  def change
    change_table :tags do |t|
      t.remove :article_id, :priority
      t.references :research
    end
  end
end
