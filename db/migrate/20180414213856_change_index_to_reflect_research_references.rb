class ChangeIndexToReflectResearchReferences < ActiveRecord::Migration[5.1]
  def change
    remove_index :publications, [:title, :publisher_id]
    add_index :publications, [:title, :publisher_id, :research_id], unique: true
  end
end
