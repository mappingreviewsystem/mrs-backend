class DropResearchArticleDatabases < ActiveRecord::Migration[5.1]
  def change
    drop_table :research_article_databases
  end
end
