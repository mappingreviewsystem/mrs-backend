class ChangeArticleExtractionQuestion < ActiveRecord::Migration[5.2]
  def change
    change_table :article_extraction_questions do |t|
      t.references :extraction_question_option, index: { name: 'index_article_e_questions_on_e_question_option_id' }
    end
  end
end
