class AddUniqueIndexToArticleReferences < ActiveRecord::Migration[5.1]
  def change
    remove_index :article_references, column: :source_id
    remove_index :article_references, column: :destination_id
    add_index :article_references, [:source_id, :destination_id], unique: true
  end
end
