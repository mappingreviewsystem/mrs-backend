class ChangeOwnerFromGroupMemberships < ActiveRecord::Migration[5.1]
  def change
    GroupMembership.update_all(owner: true)
    change_column :group_memberships, :owner, :boolean, null: false, default: false
  end
end
