class ChangeCurrentResearchFromUsers < ActiveRecord::Migration[5.1]
  def change
    rename_column :users, :current_research, :current_research_id
  end
end
