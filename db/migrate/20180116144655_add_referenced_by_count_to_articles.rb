class AddReferencedByCountToArticles < ActiveRecord::Migration[5.1]
  def change
    change_table :articles do |t|
      t.integer :referenced_by_count, default: 0
    end

    reversible do |dir|
      dir.up { data }
    end
  end

  def data
    execute <<-SQL.squish
        UPDATE articles
           SET referenced_by_count = (SELECT count(1)
                                   FROM article_references
                                  WHERE article_references.destination_id = articles.id)
    SQL
  end
end
