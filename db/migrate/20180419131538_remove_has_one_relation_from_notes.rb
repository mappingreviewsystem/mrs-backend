class RemoveHasOneRelationFromNotes < ActiveRecord::Migration[5.1]
  def change
    Note.all.find_each do |note|
      if note.note_article.present?
        note.update(article_id: note.note_article.article_id)
      elsif note.note_research.present?
        note.update(research_id: note.note_research.research_id)
      end
    end

    drop_table :note_articles
    drop_table :note_researches
  end
end
