class AddDoiToFunders < ActiveRecord::Migration[5.1]
  def change
    remove_column :authors, :doi
    change_table :funders do |t|
      t.text :doi
    end
  end
end
