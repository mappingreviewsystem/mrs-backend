class AddArticlesCountToResearches < ActiveRecord::Migration[5.2]
  def change
    change_table :researches do |t|
      t.integer :articles_count, null: false, default: 0
    end

    ActiveRecord::Base.connection.execute <<-SQL.squish
      UPDATE researches
      SET articles_count = (SELECT count(1)
                                 FROM articles
                                WHERE articles.research_id = researches.id)
    SQL
  end
end
