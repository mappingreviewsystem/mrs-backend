class MigrateArticlesAuthors < ActiveRecord::Migration[5.1]
  def change
    Article.transaction do
      Article.find_each do |article|
        article.authors = article.deprecated_authors
      end
    end
  end
end
