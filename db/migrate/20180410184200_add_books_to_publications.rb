class AddBooksToPublications < ActiveRecord::Migration[5.1]
  def change
    change_table :publications do |t|
      t.text :doi
      t.text :isbn
    end
  end
end
