class MigrateArticlesKeywords < ActiveRecord::Migration[5.1]
  def change
    Article.transaction do
      Article.find_each do |article|
        article.keywords = article.deprecated_keywords
      end
    end
  end
end
