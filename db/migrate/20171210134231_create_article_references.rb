class CreateArticleReferences < ActiveRecord::Migration[5.1]
  def change
    create_table :article_references do |t|
      t.references :source, index: true, foreign_key: { to_table: :articles }
      t.references :destination, index: true, foreign_key: { to_table: :articles }

      t.timestamps
    end
  end
end
