# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_08_21_165807) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "affiliation_authors", force: :cascade do |t|
    t.bigint "affiliation_id"
    t.bigint "author_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["affiliation_id"], name: "index_affiliation_authors_on_affiliation_id"
    t.index ["author_id"], name: "index_affiliation_authors_on_author_id"
  end

  create_table "affiliations", force: :cascade do |t|
    t.text "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "location"
    t.bigint "research_id"
    t.index ["name", "location", "research_id"], name: "index_affiliations_on_name_and_location_and_research_id", unique: true
    t.index ["research_id"], name: "index_affiliations_on_research_id"
  end

  create_table "article_authors", force: :cascade do |t|
    t.bigint "article_id"
    t.bigint "author_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["article_id", "author_id"], name: "index_article_authors_on_article_id_and_author_id", unique: true
    t.index ["article_id"], name: "index_article_authors_on_article_id"
    t.index ["author_id"], name: "index_article_authors_on_author_id"
  end

  create_table "article_databases", id: :serial, force: :cascade do |t|
    t.text "title"
    t.text "url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "query"
    t.bigint "research_id"
    t.string "exported_search_file_name"
    t.string "exported_search_content_type"
    t.integer "exported_search_file_size"
    t.datetime "exported_search_updated_at"
    t.boolean "downloaded", default: false, null: false
    t.text "missing_lines"
    t.integer "status", default: 0, null: false
    t.index ["research_id"], name: "index_article_databases_on_research_id"
    t.index ["title", "research_id"], name: "index_article_databases_on_title_and_research_id", unique: true
  end

  create_table "article_exclusion_criteria", id: :serial, force: :cascade do |t|
    t.integer "article_id"
    t.integer "exclusion_criterium_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "doubt", default: 0
    t.index ["article_id", "exclusion_criterium_id"], name: "article_exclusion_criteria_unique_index", unique: true
    t.index ["article_id"], name: "index_article_exclusion_criteria_on_article_id"
    t.index ["exclusion_criterium_id"], name: "index_article_exclusion_criteria_on_exclusion_criterium_id"
  end

  create_table "article_extraction_questions", id: :serial, force: :cascade do |t|
    t.integer "article_id"
    t.integer "extraction_question_id"
    t.text "value", default: "", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "doubt", default: 0
    t.bigint "extraction_question_option_id"
    t.index ["article_id", "extraction_question_id"], name: "article_extraction_questions_unique_index", unique: true
    t.index ["article_id"], name: "index_article_extraction_questions_on_article_id"
    t.index ["extraction_question_id"], name: "index_article_extraction_questions_on_extraction_question_id"
    t.index ["extraction_question_option_id"], name: "index_article_e_questions_on_e_question_option_id"
  end

  create_table "article_inclusion_criteria", id: :serial, force: :cascade do |t|
    t.integer "article_id"
    t.integer "inclusion_criterium_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "doubt", default: 0
    t.index ["article_id", "inclusion_criterium_id"], name: "article_inclusion_criteria_unique_index", unique: true
    t.index ["article_id"], name: "index_article_inclusion_criteria_on_article_id"
    t.index ["inclusion_criterium_id"], name: "index_article_inclusion_criteria_on_inclusion_criterium_id"
  end

  create_table "article_keywords", force: :cascade do |t|
    t.bigint "article_id"
    t.bigint "keyword_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["article_id", "keyword_id"], name: "index_article_keywords_on_article_id_and_keyword_id", unique: true
    t.index ["article_id"], name: "index_article_keywords_on_article_id"
    t.index ["keyword_id"], name: "index_article_keywords_on_keyword_id"
  end

  create_table "article_pages", id: :serial, force: :cascade do |t|
    t.integer "article_id", null: false
    t.text "value"
    t.integer "page", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["article_id", "page"], name: "index_article_pages_on_article_id_and_page", unique: true
    t.index ["article_id"], name: "index_article_pages_on_article_id"
  end

  create_table "article_publications", id: :serial, force: :cascade do |t|
    t.integer "article_id"
    t.integer "publication_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["article_id"], name: "index_article_publications_on_article_id", unique: true
    t.index ["publication_id"], name: "index_article_publications_on_publication_id"
  end

  create_table "article_references", force: :cascade do |t|
    t.bigint "source_id"
    t.bigint "destination_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "status", default: 0, null: false
    t.index ["source_id", "destination_id"], name: "index_article_references_on_source_id_and_destination_id", unique: true
  end

  create_table "article_tags", force: :cascade do |t|
    t.bigint "article_id"
    t.bigint "tag_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["article_id", "tag_id"], name: "index_article_tags_on_article_id_and_tag_id", unique: true
    t.index ["article_id"], name: "index_article_tags_on_article_id"
    t.index ["tag_id"], name: "index_article_tags_on_tag_id"
  end

  create_table "articles", id: :serial, force: :cascade do |t|
    t.text "title"
    t.text "url"
    t.integer "publication_year"
    t.text "doi"
    t.text "volume"
    t.text "issue"
    t.text "page_start"
    t.text "page_end"
    t.text "isbn"
    t.text "issn"
    t.integer "status", default: 2, null: false
    t.text "data"
    t.integer "article_database_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "file_file_name"
    t.text "file_content_type"
    t.integer "file_file_size"
    t.datetime "file_updated_at"
    t.integer "research_id"
    t.integer "referenced_by_count", default: 0, null: false
    t.integer "references_count", default: 0, null: false
    t.bigint "funder_id"
    t.integer "content_type_id", default: 1
    t.text "abstract"
    t.index ["article_database_id"], name: "index_articles_on_article_database_id"
    t.index ["doi", "research_id"], name: "index_articles_on_doi_and_research_id", unique: true, where: "(doi IS NOT NULL)"
    t.index ["funder_id"], name: "index_articles_on_funder_id"
    t.index ["title", "research_id"], name: "index_articles_on_title_and_research_id", unique: true, where: "(doi IS NULL)"
  end

  create_table "authors", id: :serial, force: :cascade do |t|
    t.text "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "given"
    t.text "family"
    t.bigint "research_id"
    t.index ["research_id"], name: "index_authors_on_research_id"
  end

  create_table "content_types", force: :cascade do |t|
    t.text "code", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["code"], name: "index_content_types_on_code", unique: true
  end

  create_table "crossrefs", force: :cascade do |t|
    t.text "doi"
    t.text "data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "issn"
    t.text "isbn"
    t.index ["doi"], name: "index_crossrefs_on_doi", unique: true, where: "(doi IS NOT NULL)"
    t.index ["isbn"], name: "index_crossrefs_on_isbn", unique: true, where: "(isbn IS NOT NULL)"
    t.index ["issn"], name: "index_crossrefs_on_issn", unique: true, where: "(issn IS NOT NULL)"
  end

  create_table "exclusion_criteria", id: :serial, force: :cascade do |t|
    t.text "title"
    t.integer "research_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["research_id"], name: "index_exclusion_criteria_on_research_id"
  end

  create_table "extraction_question_option_article_extraction_questions", force: :cascade do |t|
    t.bigint "extraction_question_option_id"
    t.bigint "article_extraction_question_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["article_extraction_question_id"], name: "index_a_e_q_on_e_q_option_a_e_q_id"
    t.index ["extraction_question_option_id"], name: "index_e_q_option_on_e_q_option_a_e_q_id"
  end

  create_table "extraction_question_options", force: :cascade do |t|
    t.bigint "extraction_question_id"
    t.text "title", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["extraction_question_id"], name: "index_extraction_question_options_on_extraction_question_id"
  end

  create_table "extraction_questions", id: :serial, force: :cascade do |t|
    t.text "key"
    t.text "value"
    t.integer "research_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "field_type", default: 0, null: false
    t.text "range_min"
    t.text "range_max"
    t.text "range_increment"
    t.index ["research_id"], name: "index_extraction_questions_on_research_id"
  end

  create_table "extraction_questions_research_questions", id: false, force: :cascade do |t|
    t.integer "extraction_question_id", null: false
    t.integer "research_question_id", null: false
  end

  create_table "funders", force: :cascade do |t|
    t.text "name"
    t.text "url"
    t.text "location"
    t.text "crossref_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "doi"
    t.bigint "research_id"
    t.index ["crossref_id"], name: "index_funders_on_crossref_id", unique: true, where: "(crossref_id IS NOT NULL)"
    t.index ["doi"], name: "index_funders_on_doi", unique: true, where: "(doi IS NOT NULL)"
    t.index ["research_id"], name: "index_funders_on_research_id"
  end

  create_table "group_memberships", id: :serial, force: :cascade do |t|
    t.integer "group_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "owner", default: false, null: false
    t.index ["group_id"], name: "index_group_memberships_on_group_id"
    t.index ["user_id", "group_id"], name: "index_group_memberships_on_user_id_and_group_id", unique: true
    t.index ["user_id"], name: "index_group_memberships_on_user_id"
  end

  create_table "groups", id: :serial, force: :cascade do |t|
    t.text "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "inclusion_criteria", id: :serial, force: :cascade do |t|
    t.text "title"
    t.integer "research_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["research_id"], name: "index_inclusion_criteria_on_research_id"
  end

  create_table "keywords", id: :serial, force: :cascade do |t|
    t.text "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "note_tags", force: :cascade do |t|
    t.bigint "note_id"
    t.bigint "tag_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["note_id"], name: "index_note_tags_on_note_id"
    t.index ["tag_id"], name: "index_note_tags_on_tag_id"
  end

  create_table "notes", force: :cascade do |t|
    t.text "value", default: "", null: false
    t.string "color", limit: 6, default: "000", null: false
    t.integer "priority", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "title"
    t.bigint "article_id"
    t.bigint "research_id"
    t.index ["article_id"], name: "index_notes_on_article_id"
    t.index ["research_id"], name: "index_notes_on_research_id"
  end

  create_table "publications", id: :serial, force: :cascade do |t|
    t.text "title"
    t.integer "publisher_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "publication_type", default: ""
    t.text "location", default: ""
    t.text "issn"
    t.text "doi"
    t.text "isbn"
    t.bigint "research_id"
    t.index ["isbn", "research_id"], name: "index_publications_on_isbn_and_research_id", unique: true, where: "(isbn IS NOT NULL)"
    t.index ["issn", "research_id"], name: "index_publications_on_issn_and_research_id", unique: true, where: "(issn IS NOT NULL)"
    t.index ["publisher_id"], name: "index_publications_on_publisher_id"
    t.index ["research_id"], name: "index_publications_on_research_id"
    t.index ["title", "publisher_id", "research_id"], name: "index_publications_on_title_and_publisher_id_and_research_id", unique: true
  end

  create_table "publishers", id: :serial, force: :cascade do |t|
    t.text "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "research_keywords", force: :cascade do |t|
    t.bigint "research_id"
    t.bigint "keyword_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["keyword_id"], name: "index_research_keywords_on_keyword_id"
    t.index ["research_id", "keyword_id"], name: "index_research_keywords_on_research_id_and_keyword_id", unique: true
    t.index ["research_id"], name: "index_research_keywords_on_research_id"
  end

  create_table "research_questions", id: :serial, force: :cascade do |t|
    t.text "title"
    t.text "description"
    t.text "related_metadata"
    t.integer "research_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "value", default: "", null: false
    t.index ["research_id"], name: "index_research_questions_on_research_id"
  end

  create_table "researches", id: :serial, force: :cascade do |t|
    t.text "title"
    t.text "objective"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
    t.integer "group_id"
    t.integer "articles_count", default: 0, null: false
  end

  create_table "tags", force: :cascade do |t|
    t.text "label", default: "", null: false
    t.string "color", limit: 6, default: "000", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "research_id"
    t.index ["research_id"], name: "index_tags_on_research_id"
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.text "email", default: "", null: false
    t.text "encrypted_password", default: "", null: false
    t.text "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.text "current_sign_in_ip"
    t.text "last_sign_in_ip"
    t.integer "role", default: 0, null: false
    t.integer "current_research_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "name"
    t.string "provider", default: "email", null: false
    t.string "uid", default: "", null: false
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.json "tokens"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true
  end

  add_foreign_key "affiliation_authors", "affiliations"
  add_foreign_key "affiliation_authors", "authors"
  add_foreign_key "article_authors", "articles"
  add_foreign_key "article_authors", "authors"
  add_foreign_key "article_keywords", "articles"
  add_foreign_key "article_keywords", "keywords"
  add_foreign_key "article_pages", "articles"
  add_foreign_key "article_publications", "articles"
  add_foreign_key "article_publications", "publications"
  add_foreign_key "article_references", "articles", column: "destination_id"
  add_foreign_key "article_references", "articles", column: "source_id"
  add_foreign_key "article_tags", "articles"
  add_foreign_key "article_tags", "tags"
  add_foreign_key "articles", "funders"
  add_foreign_key "exclusion_criteria", "researches"
  add_foreign_key "extraction_question_option_article_extraction_questions", "article_extraction_questions"
  add_foreign_key "extraction_question_option_article_extraction_questions", "extraction_question_options"
  add_foreign_key "extraction_question_options", "extraction_questions"
  add_foreign_key "extraction_questions", "researches"
  add_foreign_key "group_memberships", "groups"
  add_foreign_key "group_memberships", "users"
  add_foreign_key "inclusion_criteria", "researches"
  add_foreign_key "note_tags", "notes"
  add_foreign_key "note_tags", "tags"
  add_foreign_key "publications", "publishers"
  add_foreign_key "research_keywords", "keywords"
  add_foreign_key "research_keywords", "researches"
  add_foreign_key "research_questions", "researches"
end
