# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

acm = ArticleDatabase.create(title: 'ACM', url: 'https://dl.acm.org/')
ieee = ArticleDatabase.create(title: 'IEEE', url: 'http://ieeexplore.ieee.org/Xplore/home.jsp')
springer = ArticleDatabase.create(title: 'Springer', url: 'https://link.springer.com/')

# content type from CrossRef
ContentType.where(code: 'other').first_or_create
types = Serrano.types
if types['status'] == 'ok'
  types['message']['items'].each do |type|
    ContentType.where(code: type['id']).first_or_create
  end
end
