require_relative 'boot'

# require 'rails/all'
require "rails"

# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "active_storage/engine"
require "action_controller/railtie"
require "action_mailer/railtie"
# require "action_view/railtie"
# require "action_cable/engine"
# require "sprockets/railtie"
require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Mrs
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1
    config.api_only = true

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # default language
    config.i18n.default_locale = :pt_BR

    # sidekiq as default job processor
    config.active_job.queue_adapter = :sidekiq

    # CORS
    # TODO: fix paths to reflect production
    config.middleware.insert_before 0, Rack::Cors do
      allow do
        origins '*'
        resource '*',
          headers: :any,
          expose: %w[uid client token-type access-token expiry Accept-Ranges Content-Encoding Content-Length Content-Range],
          methods: [:get, :post, :options, :delete, :put, :patch]
      end
    end
  end
end
