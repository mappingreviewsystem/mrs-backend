Rails.application.routes.draw do
  post '/graphql', to: 'graphql#execute'
  # require 'sidekiq/web'
  # devise_for :users, controllers: { registrations: 'registrations', sessions: 'sessions' }
  # authenticate :user do
  #   mount Sidekiq::Web => '/sidekiq'
  # end
  # get '/', to: 'home#index'
  # root to: 'home#index'

  concern :api_base do
    mount_devise_token_auth_for 'User', at: 'auth', skip: [:omniauth_callbacks]
    # authenticate :user do
      get 'criteria/', to: 'criteria#index', as: 'criteria'
      resources :home, only: :index
      resources :publishers
      resources :publications
      resources :authors
      resources :keywords
      resources :groups
      resources :group_memberships
      resources :article_databases do
        member do
          get '/import', to: 'article_databases#import', as: 'import'
          # get '/download', to: 'article_databases#download', as: 'download'
        end
      end
      resources :article_extraction_questions
      resources :article_exclusion_criteria
      resources :article_inclusion_criteria
      resources :article_references
      resources :content_types, only: [:index, :show]
      resources :extraction_questions
      resources :extraction_question_options
      resources :exclusion_criteria
      resources :inclusion_criteria
      resources :research_questions
      resources :notes
      resources :tags
      resources :funders
      resources :affiliations
      resources :users, only: [:index, :show, :create, :update, :destroy]
      resources :articles do
        member do
          get '/reference_graphs', to: 'reference_graphs#show'
          get '/crossref', to: 'articles#crossref'
        end
      end
      resources :researches do
        member do
          get '/statistics', to: 'researches#statistics'
          get '/spreadsheet', to: 'researches#spreadsheet'
        end
        collection do
          resources :reference_graphs, only: :index
          resources :wizard, controller: 'researches/wizard', as: 'wizard_research'
        end
      end

      # article data extraction
      get '/articles/:article_id/data_extraction', to: 'article_data_extraction#show', as: 'data_extraction'
      get '/data_extraction(/:article_id)', to: 'article_data_extraction#unseen', as: 'unseen_data_extraction'
    # end
  end

  namespace :api do
    namespace :v1 do
      concerns :api_base
    end
  end
end
