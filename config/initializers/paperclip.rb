Paperclip::Attachment.default_options.merge!(default_url: "")

Paperclip.options[:content_type_mappings] = {
  bib: %w[text/plain]
}
