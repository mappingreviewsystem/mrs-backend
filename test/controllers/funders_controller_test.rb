require 'test_helper'

class FundersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @funder = funders(:one)
  end

  test "should get index" do
    get funders_url, as: :json
    assert_response :success
  end

  test "should create funder" do
    assert_difference('Funder.count') do
      post funders_url, params: { funder: { crossref_id: @funder.crossref_id, location: @funder.location, name: @funder.name, url: @funder.url } }, as: :json
    end

    assert_response 201
  end

  test "should show funder" do
    get funder_url(@funder), as: :json
    assert_response :success
  end

  test "should update funder" do
    patch funder_url(@funder), params: { funder: { crossref_id: @funder.crossref_id, location: @funder.location, name: @funder.name, url: @funder.url } }, as: :json
    assert_response 200
  end

  test "should destroy funder" do
    assert_difference('Funder.count', -1) do
      delete funder_url(@funder), as: :json
    end

    assert_response 204
  end
end
