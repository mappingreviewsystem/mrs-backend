require 'test_helper'

class NoteTagsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @note_tag = note_tags(:one)
  end

  test "should get index" do
    get note_tags_url, as: :json
    assert_response :success
  end

  test "should create note_tag" do
    assert_difference('NoteTag.count') do
      post note_tags_url, params: { note_tag: { note_id: @note_tag.note_id, tag_id: @note_tag.tag_id } }, as: :json
    end

    assert_response 201
  end

  test "should show note_tag" do
    get note_tag_url(@note_tag), as: :json
    assert_response :success
  end

  test "should update note_tag" do
    patch note_tag_url(@note_tag), params: { note_tag: { note_id: @note_tag.note_id, tag_id: @note_tag.tag_id } }, as: :json
    assert_response 200
  end

  test "should destroy note_tag" do
    assert_difference('NoteTag.count', -1) do
      delete note_tag_url(@note_tag), as: :json
    end

    assert_response 204
  end
end
