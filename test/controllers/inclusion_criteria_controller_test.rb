require 'test_helper'

class InclusionCriteriaControllerTest < ActionDispatch::IntegrationTest
  setup do
    @inclusion_criterium = inclusion_criteria(:one)
  end

  test "should get index" do
    get inclusion_criteria_url
    assert_response :success
  end

  test "should get new" do
    get new_inclusion_criterium_url
    assert_response :success
  end

  test "should create inclusion_criterium" do
    assert_difference('InclusionCriterium.count') do
      post inclusion_criteria_url, params: { inclusion_criterium: { research_id: @inclusion_criterium.research_id, title: @inclusion_criterium.title } }
    end

    assert_redirected_to inclusion_criterium_url(InclusionCriterium.last)
  end

  test "should show inclusion_criterium" do
    get inclusion_criterium_url(@inclusion_criterium)
    assert_response :success
  end

  test "should get edit" do
    get edit_inclusion_criterium_url(@inclusion_criterium)
    assert_response :success
  end

  test "should update inclusion_criterium" do
    patch inclusion_criterium_url(@inclusion_criterium), params: { inclusion_criterium: { research_id: @inclusion_criterium.research_id, title: @inclusion_criterium.title } }
    assert_redirected_to inclusion_criterium_url(@inclusion_criterium)
  end

  test "should destroy inclusion_criterium" do
    assert_difference('InclusionCriterium.count', -1) do
      delete inclusion_criterium_url(@inclusion_criterium)
    end

    assert_redirected_to inclusion_criteria_url
  end
end
