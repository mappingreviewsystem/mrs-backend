require 'test_helper'

class ResearchQuestionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @research_question = research_questions(:one)
  end

  test "should get index" do
    get research_questions_url
    assert_response :success
  end

  test "should get new" do
    get new_research_question_url
    assert_response :success
  end

  test "should create research_question" do
    assert_difference('ResearchQuestion.count') do
      post research_questions_url, params: { research_question: { description: @research_question.description, related_metadata: @research_question.related_metadata, research_id: @research_question.research_id, title: @research_question.title } }
    end

    assert_redirected_to research_question_url(ResearchQuestion.last)
  end

  test "should show research_question" do
    get research_question_url(@research_question)
    assert_response :success
  end

  test "should get edit" do
    get edit_research_question_url(@research_question)
    assert_response :success
  end

  test "should update research_question" do
    patch research_question_url(@research_question), params: { research_question: { description: @research_question.description, related_metadata: @research_question.related_metadata, research_id: @research_question.research_id, title: @research_question.title } }
    assert_redirected_to research_question_url(@research_question)
  end

  test "should destroy research_question" do
    assert_difference('ResearchQuestion.count', -1) do
      delete research_question_url(@research_question)
    end

    assert_redirected_to research_questions_url
  end
end
