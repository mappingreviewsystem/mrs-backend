require 'test_helper'

class ArticleDatabasesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @article_database = article_databases(:one)
  end

  test "should get index" do
    get article_databases_url
    assert_response :success
  end

  test "should get new" do
    get new_article_database_url
    assert_response :success
  end

  test "should create article_database" do
    assert_difference('ArticleDatabase.count') do
      post article_databases_url, params: { article_database: { title: @article_database.title, url: @article_database.url } }
    end

    assert_redirected_to article_database_url(ArticleDatabase.last)
  end

  test "should show article_database" do
    get article_database_url(@article_database)
    assert_response :success
  end

  test "should get edit" do
    get edit_article_database_url(@article_database)
    assert_response :success
  end

  test "should update article_database" do
    patch article_database_url(@article_database), params: { article_database: { title: @article_database.title, url: @article_database.url } }
    assert_redirected_to article_database_url(@article_database)
  end

  test "should destroy article_database" do
    assert_difference('ArticleDatabase.count', -1) do
      delete article_database_url(@article_database)
    end

    assert_redirected_to article_databases_url
  end
end
