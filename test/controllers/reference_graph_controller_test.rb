require 'test_helper'

class ReferenceGraphControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get reference_graph_new_url
    assert_response :success
  end

  test "should get create" do
    get reference_graph_create_url
    assert_response :success
  end

  test "should get show" do
    get reference_graph_show_url
    assert_response :success
  end

end
