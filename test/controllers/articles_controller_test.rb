require 'test_helper'

class ArticlesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @article = articles(:one)
  end

  test "should get index" do
    get articles_url
    assert_response :success
  end

  test "should get new" do
    get new_article_url
    assert_response :success
  end

  test "should create article" do
    assert_difference('Article.count') do
      post articles_url, params: { article: { data: @article.data, doi: @article.doi, isbn: @article.isbn, issn: @article.issn, issue: @article.issue, page_end: @article.page_end, page_start: @article.page_start, publication_year: @article.publication_year, title: @article.title, type: @article.type, url: @article.url, volume: @article.volume } }
    end

    assert_redirected_to article_url(Article.last)
  end

  test "should show article" do
    get article_url(@article)
    assert_response :success
  end

  test "should get edit" do
    get edit_article_url(@article)
    assert_response :success
  end

  test "should update article" do
    patch article_url(@article), params: { article: { data: @article.data, doi: @article.doi, isbn: @article.isbn, issn: @article.issn, issue: @article.issue, page_end: @article.page_end, page_start: @article.page_start, publication_year: @article.publication_year, title: @article.title, type: @article.type, url: @article.url, volume: @article.volume } }
    assert_redirected_to article_url(@article)
  end

  test "should destroy article" do
    assert_difference('Article.count', -1) do
      delete article_url(@article)
    end

    assert_redirected_to articles_url
  end
end
