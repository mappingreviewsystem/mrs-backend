require 'test_helper'

class ExclusionCriteriaControllerTest < ActionDispatch::IntegrationTest
  setup do
    @exclusion_criterium = exclusion_criteria(:one)
  end

  test "should get index" do
    get exclusion_criteria_url
    assert_response :success
  end

  test "should get new" do
    get new_exclusion_criterium_url
    assert_response :success
  end

  test "should create exclusion_criterium" do
    assert_difference('ExclusionCriterium.count') do
      post exclusion_criteria_url, params: { exclusion_criterium: { research_id: @exclusion_criterium.research_id, title: @exclusion_criterium.title } }
    end

    assert_redirected_to exclusion_criterium_url(ExclusionCriterium.last)
  end

  test "should show exclusion_criterium" do
    get exclusion_criterium_url(@exclusion_criterium)
    assert_response :success
  end

  test "should get edit" do
    get edit_exclusion_criterium_url(@exclusion_criterium)
    assert_response :success
  end

  test "should update exclusion_criterium" do
    patch exclusion_criterium_url(@exclusion_criterium), params: { exclusion_criterium: { research_id: @exclusion_criterium.research_id, title: @exclusion_criterium.title } }
    assert_redirected_to exclusion_criterium_url(@exclusion_criterium)
  end

  test "should destroy exclusion_criterium" do
    assert_difference('ExclusionCriterium.count', -1) do
      delete exclusion_criterium_url(@exclusion_criterium)
    end

    assert_redirected_to exclusion_criteria_url
  end
end
