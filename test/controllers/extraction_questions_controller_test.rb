require 'test_helper'

class ExtractionQuestionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @extraction_question = extraction_questions(:one)
  end

  test "should get index" do
    get extraction_questions_url
    assert_response :success
  end

  test "should get new" do
    get new_extraction_question_url
    assert_response :success
  end

  test "should create extraction_question" do
    assert_difference('ExtractionQuestion.count') do
      post extraction_questions_url, params: { extraction_question: { key: @extraction_question.key, research_id: @extraction_question.research_id, value: @extraction_question.value } }
    end

    assert_redirected_to extraction_question_url(ExtractionQuestion.last)
  end

  test "should show extraction_question" do
    get extraction_question_url(@extraction_question)
    assert_response :success
  end

  test "should get edit" do
    get edit_extraction_question_url(@extraction_question)
    assert_response :success
  end

  test "should update extraction_question" do
    patch extraction_question_url(@extraction_question), params: { extraction_question: { key: @extraction_question.key, research_id: @extraction_question.research_id, value: @extraction_question.value } }
    assert_redirected_to extraction_question_url(@extraction_question)
  end

  test "should destroy extraction_question" do
    assert_difference('ExtractionQuestion.count', -1) do
      delete extraction_question_url(@extraction_question)
    end

    assert_redirected_to extraction_questions_url
  end
end
