require 'test_helper'

class ResearchQuestionOptionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @research_question_option = research_question_options(:one)
  end

  test "should get index" do
    get research_question_options_url, as: :json
    assert_response :success
  end

  test "should create research_question_option" do
    assert_difference('ResearchQuestionOption.count') do
      post research_question_options_url, params: { research_question_option: { research_question_id: @research_question_option.research_question_id, title: @research_question_option.title } }, as: :json
    end

    assert_response 201
  end

  test "should show research_question_option" do
    get research_question_option_url(@research_question_option), as: :json
    assert_response :success
  end

  test "should update research_question_option" do
    patch research_question_option_url(@research_question_option), params: { research_question_option: { research_question_id: @research_question_option.research_question_id, title: @research_question_option.title } }, as: :json
    assert_response 200
  end

  test "should destroy research_question_option" do
    assert_difference('ResearchQuestionOption.count', -1) do
      delete research_question_option_url(@research_question_option), as: :json
    end

    assert_response 204
  end
end
