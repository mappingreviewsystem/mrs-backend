module ArticleDownloader
  def self.download(params)
    Downloader.new(params).download
  end
end
