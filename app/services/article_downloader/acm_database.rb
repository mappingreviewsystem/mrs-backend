module ArticleDownloader
  class ACMDatabase < Database
    def self.download_article(article)
      agent = super
      # logged_in(agent)
      page = agent.get(article.url)
      sleep(rand(5))
      pdf = page.link_with(text: /PDF/)
      return nil if pdf.try(:uri).nil?

      file = agent.get pdf.uri
      filename = "#{file.find_free_name(nil).split('.pdf').first}.pdf"
      file.save("/tmp/#{filename}")
    end

    # TODO: create a better login logic to handle multiple users
    # def self.logged_in(agent)
    #   if File.exist?("/tmp/acmcookies.yaml")
    #     agent.cookie_jar.load("/tmp/acmcookies.yaml")
    #   else
    #     login_page = agent.get('https://dl.acm.org/signin.cfm')
    #     login_page.form_with(id: 'form-signlogin') do |f|
    #       f.uname = ''
    #       f.pword = ''
    #     end.click_button
    #     agent.cookie_jar.save("/tmp/acmcookies.yaml")
    #   end
    # end
  end
end
