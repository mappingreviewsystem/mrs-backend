module ArticleDownloader
  class IEEEDatabase < Database
    def self.download_article(article)
      agent = super
      file = agent.get(article.url)
      filename = "#{file.find_free_name(nil).split('.pdf').first}.pdf"
      file.save("/tmp/#{filename}")
    end
  end
end
