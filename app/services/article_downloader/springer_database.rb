module ArticleDownloader
  class SpringerDatabase < Database
    def self.download_article(article)
      agent = super
      page = agent.get(article.url)
      pdf = page.link_with(href: /content\/pdf/)
      return nil if pdf.blank?

      file = agent.get pdf.uri
      filename = "#{file.find_free_name(nil).split('.pdf').first}.pdf"
      file.save("/tmp/#{filename}")
    end
  end
end
