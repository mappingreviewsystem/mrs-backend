module ArticleDownloader
  class Downloader
    def initialize(params)
      @article = params[:article]
    end

    def download
      ArticleDownloaderWorker.perform_async(@article.id)
    end
  end
end
