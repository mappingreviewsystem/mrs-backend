module ResearchSpreadsheet
  def self.generate(research)
    Spreadsheet.new(research: research).generate
  end
end
