module ArticleImporter
  # TODO: add serrano to import data, and a separate service to do it
  def self.import(params)
    ArticleImporterWorker.perform_async(params[:article_database].try(:id), params[:download])
  end

  def self.import_sync(params)
    Importer.new(params).import
  end
end
