module RecordCleanerService
  def self.clean_all
    {
      authors: authors,
      affiliations: affiliations,
      keywords: keywords,
      notes: notes,
      publications: publications,
      publishers: publishers
    }
  end

  def self.authors
    Author.where(clean_date).find_each { |author| author.destroy if author.articles.empty? }
  end

  def self.affiliations
    Affiliation.where(clean_date).find_each { |af| af.destroy if af.authors.empty? }
  end

  def self.keywords
    Keyword.where(clean_date).find_each { |k| k.destroy if k.articles.empty? && k.researches.empty? }
  end

  def self.notes
    Note.where(clean_date).find_each { |note| note.destroy if note.article.nil? && note.research.nil? }
  end

  def self.publications
    Publication.where(clean_date).find_each { |pub| pub.destroy if pub.articles.empty? }
  end

  def self.publishers
    Publisher.where(clean_date).find_each { |pub| pub.destroy if pub.publications.empty? }
  end

  # clean records updated at ...
  def self.clean_date
    ['updated_at < ?', 7.days.ago]
  end
end
