module ArticleCrossrefService
  # TODO: add serrano to import data, and a separate service to do it
  def self.crossref(article_id, references = false, force = false)
    ArticleCrossrefWorker.perform_async(article_id, references, force)
  end

  def self.crossref_sync(article_id, references = false, force = false)
    article = Article.find(article_id) unless article_id.blank?
    return true if article.nil? || (article.doi.blank? && article.issn.blank? && article.isbn.blank?)
    Crossref.new(article, references, force).import
  end
end
