module ArticleCrossrefService
  class Crossref
    def initialize(article, references = false, force = false)
      @article = article
      @references = references
      @force = force
    end

    def import
      doi_result = doi(@force)
      {
        doi: doi_result,
        issn: issn(nil, @force),
        isbn: isbn(nil, @force),
        funder: funder(nil, @force)
      }
    end

    def doi(force = false)
      data = ::Crossref.find_by(doi: @article.doi).try(:data_hash)

      if (data.nil? || force) && !@article.doi.blank?
        begin
          data = Serrano.works(ids: @article.doi)[0]
          ::Crossref.create(doi: @article.doi, data: JSON.generate(data['message'])) if data['status'] == 'ok'
          data = data.try(:[], 'message')
        rescue Serrano::NotFound
          ::Crossref.create(doi: @article.doi, data: JSON.generate({}))
          data = nil
        end
      end

      doi_mapper(data)
      @article.save
    end

    def issn(issn = nil, force = false)
      return true if issn.blank? && @article.issn.blank?
      issn ||= @article.issn
      issn = ::Crossref.normalize_issn(issn)

      publication = Publication.where(issn: @article.issn, research: @article.research).first

      if publication.nil? || force
        article_data = ::Crossref.find_by(doi: @article.doi).try(:data_hash)

        @journal = []
        begin
          journal = ::Crossref.find_by(issn: issn).try(:data_hash)
          journal = Serrano.journals(ids: issn).first if journal.nil?
          if journal['status'] == 'ok'
            @journal = journal.try(:[], 'message')
            ::Crossref.create(issn: issn, data: JSON.generate(journal))
          else
            @journal = { 'title' => article_data['container-title'], 'ISSN' => [@article.issn] || article_data['ISSN'], 'publisher' => article_data['publisher'] }
          end
        rescue Serrano::NotFound
          ::Crossref.create(issn: issn, data: JSON.generate({}))
        else
          journal = issn_mapper(@journal || article_data)
          journal.save unless journal.nil?
        end
      end
      # return true if !@article.journal.try(:crossref_data).blank? && !force

      @article.publication = journal
      @article.save
    end

    def isbn(isbn = nil, force = false)
      return true if isbn.blank? && @article.isbn.blank?
      publication = Publication.where(isbn: @article.isbn, research: @article.research).first

      if publication.nil? || force
        @book = nil
        begin
          article_data = ::Crossref.find_by(doi: @article.doi).try(:data_hash)
          data = ::Crossref.find_by(isbn: ::Crossref.normalize_isbn(@article.isbn))

          if data.nil?
            data = Serrano.works(filter: { isbn: @article.isbn })
            if data['status'] == 'ok' && !article_data.blank? && !article_data.try(:[], 'container-title').blank?
              @book = data['message']['items'].find { |b| b['title'] == article_data['container-title'] } unless data['message']['items'].empty?
              @book = { 'title' => article_data['container-title'], 'ISBN' => [@article.isbn] || article_data['ISBN'], 'publisher' => article_data['publisher'] } if @book.nil? && !article_data.blank?

              ref = ::Crossref.find_by(doi: @book['DOI'])
              if ref.nil?
                ::Crossref.create(doi: @book['DOI'], isbn: ::Crossref.normalize_isbn(@article.isbn) || @article.isbn, data: JSON.generate(Serrano.works(ids: @book['DOI'])))
              else
                ref.update(isbn: ::Crossref.normalize_isbn(@article.isbn))
              end
            end
          end
        rescue Serrano::NotFound
          ::Crossref.create(isbn: ::Crossref.normalize_isbn(@article.isbn) || @article.isbn, data: JSON.generate({}))
        else
          publication = isbn_mapper(@book)
          publication.save unless publication.nil?
        end
      end

      @article.publication = publication unless publication.nil? && !force
      @article.save
    end

    def funder(doi = nil, force = false)
      funder = Funder.find_by(doi: doi, research: @article.research)
      @funder = nil

      if funder.nil? || force
        begin
          article_data = ::Crossref.find_by(doi: @article.doi).try(:data_hash)
          if doi.blank? && !article_data.blank?
            doi = article_data['funder'].first['DOI'] if article_data['funder'].is_a?(Array)
          end
          data = ::Crossref.find_by(doi: doi).try(:data_hash) unless doi.blank?

          if data.nil?
            data = Serrano.funders(ids: doi.to_s).try(:first)
            ::Crossref.create(doi: doi.to_s, data: JSON.generate(data['message'])) if data['status'] == 'ok'
            data = data['message']
          end
        rescue Serrano::NotFound
          ::Crossref.create(doi: doi, data: JSON.generate({}))
        else
          @funder = funder_mapper(data)
          @funder.save
        end
      end

      @article.funder = @funder || funder
      @article.save
    end

    private

    def doi_mapper(data)
      return true if data.blank?

      # article's metadata only
      publication_year = data['issued'].try(:[], 'date-parts').try(:flatten).try(:first)
      article_params = {
        title: data['title'].try(:first),
        content_type: ContentType.find_by_code(data['type']),
        publication_year: publication_year,
        url: data['URL'],
        isbn: data['ISBN'].try(:first),
        issn: data['ISSN'].try(:first)
      }.compact
      @article.assign_attributes(article_params)

      # authors
      Array(data['author']).each do |a|
        name = "#{a['given']} #{a['family']}".squish || a['name'].squish
        author = Author.find_by(name: name, research: @article.research)
        author = Author.new(name: name, given: a['given'], family: a['family'], research: @article.research) if author.blank?
        a['affiliation'].each do |af|
          af = Affiliation.find_or_create_by(name: af['name'].squish, research: @article.research)
          author.affiliations << af unless author.affiliations.include?(af)
        end

        @article.authors << author unless @article.authors.include?(author) || author.name.blank?
      end

      # references
      if @references
        Array(data['reference']).each do |ref|
          if !ref['DOI'].blank?
            doi = ref['DOI'].is_a?(Array) ? ref['DOI'].first : ref['DOI']
            reference = Article.find_by(doi: doi, research: @article.research)
            reference = Article.create(doi: doi, title: ref['unstructured'], research: @article.research, status: 'not_included') if reference.nil?
          else
            reference = Article.find_by(title: ref['unstructured'], research: @article.research)
            reference = Article.search(ref['unstructured'], fields: [:title, :publication_title, :publication_year, :authors], where: { research_id: @article.research_id }).first if reference.nil?
            if reference.nil?
              publication_title = ref['journal-title'].is_a?(Array) ? ref['journal-title'].first : ref['journal-title']
              publication = Publication.where(title: publication_title, research: @article.research).first_or_create unless ref['journal-title'].blank?
              params = {
                research: @article.research,
                title: ref['unstructured'],
                publication_year: ref['year'],
                volume: ref['volume'],
                issue: ref['issue'],
                page_start: ref['first-page'],
                status: 'not_included',
                publication: publication
              }.compact
              reference = Article.create(params)
            end
          end
          author = Author.find_or_create_by(name: ref['author'], research: @article.research) unless ref['author'].nil?
          reference.authors << author unless author.nil? || reference.authors.include?(author)

          ArticleReference.find_or_create_by(source_id: @article.id, destination_id: reference.id)

          ArticleCrossrefWorker.perform_async(reference.id)
        end
      end
    end

    def issn_mapper(data)
      return nil if data.blank?

      title = data['title'].is_a?(Array) ? data['title'].first : data['title']
      attributes = { title: title, publication_type: 'journal', issn: ::Crossref.normalize_issn(data['ISSN'].try(:first)) }.compact
      publication_mapper(title, attributes, data['publisher'])
    end

    def isbn_mapper(data)
      return nil if data.blank?

      title = data['title'].is_a?(Array) ? data['title'].first : data['title']
      attributes = { title: title, publication_type: data['type'], isbn: ::Crossref.normalize_isbn(data['ISBN'].try(:first)), doi: data['DOI'] }.compact
      publication_mapper(title, attributes, data['publisher'])
    end

    def publication_mapper(title, attributes, publisher)
      return nil unless title

      publisher = Publisher.find_or_create_by(name: publisher.squish) unless publisher.blank?
      publication = Publication.where('title LIKE ? AND publisher_id = ? AND research_id = ?', title, publisher.try(:id), @article.research_id).first
      publication = Publication.new if publication.nil?
      attributes[:research] = @article.research
      publication.assign_attributes(attributes)
      publication.publisher = publisher

      publication
    end

    def funder_mapper(data)
      funder = Funder.find_by(crossref_id: data['id'], research: @article.research) || Funder.find_by(name: data['name'])
      funder = Funder.create(name: data['name'], url: data['uri'], location: data['location'], crossref_id: data['id'], research: @article.research) if funder.nil?

      funder
    end
  end
end
