module ResearchSpreadsheet
  class ExcludedArticles < ArticleSheet
    def build_rows
      build_article_rows(@research.excluded_articles)
    end

    private

    def name
      t('research', 'excluded_articles')
    end

    def orientation
      'horizontal'
    end
  end
end
