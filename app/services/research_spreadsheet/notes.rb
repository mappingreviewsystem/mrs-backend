module ResearchSpreadsheet
  class Notes < Sheet
    def build_rows
      rows = []

      @research.notes.find_each do |note|
        rows << [note.title, clean_html_tags(note.value)]
      end
      rows
    end

    private

    def name
      t('research', 'notes')
    end

    def orientation
      'vertical'
    end
  end
end
