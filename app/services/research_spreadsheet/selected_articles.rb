module ResearchSpreadsheet
  class SelectedArticles < ArticleSheet
    def build_rows
      build_article_rows(@research.selected_articles, true)
    end

    private

    def name
      t('research', 'selected_articles')
    end

    def orientation
      'horizontal'
    end
  end
end
