module ResearchSpreadsheet
  class Sheet
    def initialize(params)
      @research = params[:research]
      @spreadsheet = params[:spreadsheet]
      @workbook = @spreadsheet.workbook
      @default_styles = params[:default_styles]
    end

    def generate(&block)
      @rows_count = 0

      @workbook.add_worksheet(name: name) do |new_sheet|
        rows = build_rows
        rows.each do |r|
          new_sheet.add_row(r, style: @default_styles[:top_align])
        end

        @rows_count = new_sheet.rows.size

        # header style
        position = orientation == 'vertical' ? :col_style : :row_style
        new_sheet.send(position, 0, @default_styles[:header]) if @rows_count.positive?

        froze_header(new_sheet) if @rows_count.positive?

        block if block_given?
      end
    end

    def build_rows; raise 'Must implement'; end

    private

    def name; raise 'Must implement'; end
    def orientation; raise 'Must implement'; end

    def top_left_cell
      'A1'
    end

    def t(model, attribute)
      I18n.t("activerecord.attributes.#{model}.#{attribute}")
    end

    def froze_header(sheet)
      sheet.sheet_view.pane do |pane|
        pane.top_left_cell = top_left_cell
        pane.state = :frozen_split
        pane.y_split = 1 unless orientation == 'vertical'
        pane.x_split = 1
        pane.active_pane = :bottom_right
      end
    end

    def clean_html_tags(value)
      value.gsub!(/(<br>|<br.\/>|<\/div>|<\/li>)/i, "\\1\n")
      value.gsub(/(<div>)/i, "\n\\1")
    end

    def relationships_build(relationships, model)
      rows = []

      relationships.each do |relationship, value|
        values = value.call(model)
        values = Hash[relationship, values] if values.is_a?(Array)

        values.each do |k, v|
          v = [v] unless v.is_a?(Array)
          rows << [k].concat(v)
        end
      end

      rows
    end

    # def column_name(number)
    #   dividend = number
    #   name = ''
    #
    #   while dividend.positive?
    #     modulo = (dividend - 1) % 26
    #     name = ('A'.ord + modulo).chr + name
    #     dividend = ((dividend - modulo) / 26).to_i
    #   end
    #   name
    # end
  end
end
