module ResearchSpreadsheet
  class ArticleSheet < Sheet
    def build_article_rows(articles, include_extraction = false)
      rows = []

      @articles_headers = [:id, :title, :publication_year, :content_type, :status, :authors, :url, :article_database, :doi, :publication, :issn, :isbn, :volume, :issue, :page_start, :page_end]
      @inclusion_criteria = @research.inclusion_criteria.order(:title)
      @exclusion_criteria = @research.exclusion_criteria.order(:title)
      @extraction_questions = @research.extraction_questions.order(:key)

      rows << articles_headers(include_extraction)

      articles = articles.includes(:article_extraction_questions) if include_extraction
      articles.find_each do |article|
        row = [
          article.id,
          article.title,
          article.publication_year,
          t('article.content_types', article.content_type.try(:code)),
          t('article.statuses', article.status),
          article.authors_list.join('; '),
          article.url,
          article.article_database.try(:title),
          article.doi,
          article.publication.try(:title),
          article.publication.try(:issn),
          article.publication.try(:isbn),
          article.volume,
          article.issue,
          article.page_start,
          article.page_end
        ]

        row.concat criteria_build(@inclusion_criteria, article.article_inclusion_criteria, 'inclusion_criterium_id')
        row.concat criteria_build(@exclusion_criteria, article.article_exclusion_criteria, 'exclusion_criterium_id')

        if include_extraction && !@extraction_questions.blank?
          ae_questions = article.article_extraction_questions.map { |aeq| [aeq.extraction_question_id, aeq] }.to_h
          questions = @extraction_questions.map do |question|
            value = ae_questions[question.id].try(:value)
            value = value.blank? ? '' : clean_html_tags(value)
            value
          end
          row.concat questions
        end

        row << ''
        article.notes.find_each do |note|
          row << "'#{note.title}'\n#{clean_html_tags(note.value)}"
        end

        rows << row
      end

      rows
    end

    private

    def articles_headers(include_extraction)
      headers = @articles_headers.map { |header| t('article', header) }

      headers.concat(@inclusion_criteria.map { |o| header_block('inclusion_criterium', o[:title]) })
      headers.concat(@exclusion_criteria.map { |o| header_block('exclusion_criterium', o[:title]) })
      if include_extraction && !@extraction_questions.blank?
        headers.concat(@extraction_questions.map { |o| header_block('extraction_question', o[:key]) })
      end

      headers.push(t('research', 'notes') + ' >')

      headers
    end

    def header_block(model, value)
      I18n.t("activerecord.models.#{model}.one") + "\n" + value
    end

    def criteria_build(research_criteria, article_criteria, criterium_name)
      article_criteria = article_criteria.map { |ac| [ac.send(criterium_name), ac] }.to_h
      research_criteria.map do |rc|
        value = article_criteria[rc.id]
        value = value.blank? ? '' : 'x'
        value
      end
    end
  end
end
