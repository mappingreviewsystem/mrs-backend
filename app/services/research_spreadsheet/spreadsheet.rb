module ResearchSpreadsheet
  class Spreadsheet
    attr_reader :research, :spreadsheet

    def initialize(params)
      @research = params[:research]
      @spreadsheet = params[:spreadsheet] || Axlsx::Package.new
    end

    def generate
      @spreadsheet.workbook.styles do |s|
        header = s.add_style(
          b: true,
          border: { style: :thin, color: '00' },
          alignment: { horizontal: :left, vertical: :top, wrap_text: true }
        )
        top_align = s.add_style(
          alignment: { horizontal: :left, vertical: :top }
        )
        @default_styles = {
          header: header,
          top_align: top_align
        }

        sheets = [
          Protocol.new(sheet_params),
          ResearchQuestions.new(sheet_params),
          Notes.new(sheet_params),
          SelectedArticles.new(sheet_params),
          ExcludedArticles.new(sheet_params)
        ]

        sheets.each(&:generate)
      end

      @spreadsheet
    end

    private

    def sheet_params
      {
        research: @research,
        spreadsheet: @spreadsheet,
        default_styles: @default_styles
      }
    end
  end
end
