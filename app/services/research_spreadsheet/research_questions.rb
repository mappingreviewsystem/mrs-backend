module ResearchSpreadsheet
  class ResearchQuestions < Sheet
    def build_rows
      relationship = {
        research_questions_value: ->(research) { research.research_questions.order(:title).map { |q| [q.title, clean_html_tags(q.value)] }.to_h },
      }
      relationships_build(relationship, @research)
    end

    private

    def name
      t('research', 'research_questions')
    end

    def orientation
      'vertical'
    end
  end
end
