module ResearchSpreadsheet
  class Protocol < Sheet
    def build_rows
      rows = []

      metadata = [:title, :objective]
      metadata.each { |meta| rows << [t('research', meta), @research.send(meta)] }

      relationships = {
        t('research', 'keywords') => ->(research) { research.keywords.order(:title).map(&:title) },
        t('research', 'research_questions') => ->(research) { research.research_questions.order(:title).map(&:title) },
        t('research', 'inclusion_criteria') => ->(research) { research.inclusion_criteria.order(:title).map(&:title) },
        t('research', 'exclusion_criteria') => ->(research) { research.exclusion_criteria.order(:title).map(&:title) },
        t('research', 'extraction_questions') => ->(research) { research.extraction_questions.order(:key).map { |q| "#{q.key} (#{q.value})" } }
      }
      rows.concat relationships_build(relationships, @research)

      rows
    end

    private

    def name
      t('research', 'protocol')
    end

    def orientation
      'vertical'
    end
  end
end
