module ArticleImporter
  class Database
    def self.skip_lines
      nil
    end

    def self.get_authors(research, authors, split_rule = ',')
      return [] if authors.blank?
      authors.split(split_rule).map do |author|
        next if author.length < 2
        Author.find_or_create_by(name: author.squeeze(' '), research: research)
      end
    end

    def self.get_publication(research, publication, publisher)
      return nil if publication.blank?
      publisher = nil if publisher.blank?

      pub = Publication.find_by(title: publication, research: research)
      if pub.nil?
        publisher = Publisher.find_or_create_by(name: publisher) unless publisher.nil?
        pub = Publication.create(title: publication, research: research, publisher: publisher)
      end
      pub
    end

    def self.get_keywords(keywords)
      return [] if keywords.blank?
      keywords.split(/;|,/).compact.reject(&:empty?).map do |keyword|
        Keyword.find_or_create_by(title: keyword)
      end
    end
  end
end
