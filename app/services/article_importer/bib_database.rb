module ArticleImporter
  class BibDatabase < Database
    def self.fields(item, research, article_database)
      authors = get_authors(research, item['author'].to_s, ' and ')

      publication_name = item['booktitle'] || item['journal']
      publication = get_publication(research, publication_name.to_s, item['publisher'].to_s)

      keywords = get_keywords(item['keywords'].to_s)

      pages = item['pages'].to_s.scan(/(\d+)[^0-9]*(\d+)/).first unless item['pages'].blank?
      pages ||= [nil, nil]

      {
        title: item['title'].to_s,
        # content_type: item['type'],
        url: item['url'].to_s,
        publication_year: item['year'].to_s,
        doi: item['doi'].to_s,
        volume: item['volume'].to_s,
        issue: item['issue'].to_s,
        page_start: pages.first,
        page_end: pages.last,
        isbn: item['isbn'].to_s,
        issn: item['issn'].to_s,
        abstract: item['abstract'].to_s,
        data: item.to_json,
        keywords: keywords,
        authors: authors,
        research_id: research.id,
        publication: publication,
        article_database: article_database
      }
    end
  end
end
