module ArticleImporter
  class ACMDatabase < Database
    def self.fields(row, research, article_database)
      authors = get_authors(research, row['author'], ' and ')
      publication_name = row['journal'].empty? ? row['booktitle'] : row['journal']
      publication = get_publication(research, publication_name, row['publisher'])
      keywords = get_keywords(row['keywords'])
      pages = row['pages'].split('--')

      {
        title: row['title'],
        # content_type: row['type'],
        url: "https://dl.acm.org/citation.cfm?id=#{row['id']}",
        publication_year: row['year'],
        doi: row['doi'],
        volume: row['volume'],
        issue: row['issue_no'],
        page_start: pages.first,
        page_end: pages.last,
        isbn: row['isbn'],
        issn: row['issn'],
        data: row.to_json,
        keywords: keywords,
        authors: authors,
        research_id: research.id,
        publication: publication,
        article_database: article_database
      }
    end
  end
end
