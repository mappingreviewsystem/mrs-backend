module ArticleImporter
  class IEEEDatabase < Database
    def self.fields(row, research, article_database)
      authors = get_authors(research, row['Authors'], '; ')
      publication = get_publication(research, row['Publication Title'], row['Publisher'])
      keywords = get_keywords("#{row['Author Keywords']};#{row['IEEE Terms']}")

      {
        research_id: research.id,
        title: row["Document Title"],
        # content_type: 'Article',
        url: row['PDF Link'],
        publication_year: row['Year'],
        doi: row['DOI'],
        volume: row['Volume'],
        issue: row['Issue'],
        page_start: row['Start Page'],
        page_end: row['End Page'],
        isbn: row['ISBNs'],
        issn: row['ISSN'],
        data: row.to_json,
        authors: authors,
        keywords: keywords,
        publication: publication,
        article_database: article_database
      }
    end

    def self.skip_lines
      %r{^"http:\/\/ieeexplore.ieee.org\/search\/searchresult.jsp}
    end
  end
end
