module ArticleImporter
  class SpringerDatabase < Database
    def self.fields(row, research, article_database)
      authors = get_authors(research, row['Authors'], /(?<=([a-z]))(?=[A-Z][^A-Z])/).compact#.reject { |a| a.length < 2 }
      publication = get_publication(research, row['Publication Title'], 'Springer')
      {
        research_id: research.id,
        title: row['Item Title'],
        authors: authors,
        # content_type: row['Content Type'],
        url: row['URL'],
        publication_year: row['Publication Year'],
        doi: row['Item DOI'],
        volume: row['Journal Volume'],
        issue: row['Journal Issue'],
        data: row.to_json,
        publication: publication,
        article_database: article_database
      }
    end
  end
end
