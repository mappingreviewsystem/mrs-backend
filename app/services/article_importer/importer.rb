module ArticleImporter
  class Importer
    require 'csv'
    def initialize(params)
      @article_database = params[:article_database]
      @research = params[:research] || @article_database.research
      @file = params[:file] || @article_database.exported_search.path
      @database = params[:database] || @article_database.title
      @download = params[:download] || false
    end

    def import
      Article.transaction do
        values = @file.split('.').last == 'bib' ? import_bib : import_csv

        # TODO: optimize article creation (but cares about validation)
        articles = []
        missing_lines = []
        values.each do |a|
          article = Article.new(a)
          article.save
          articles << article
        end
        articles.each_with_index { |a, i| missing_lines << "#{i + 1}: #{a.errors.as_json}" unless a.errors.blank? }
        @article_database.update(status: :imported, missing_lines: missing_lines.join(';'))

      end

      Article.where(research: @article_database.research, article_database: @article_database).each { |a| after_import(a) }

      true
    end

    private

    def import_csv
      database = allow_database(@database)
      values = []
      CSV.foreach(@file, headers: true, skip_lines: database.skip_lines) do |row|
        values << database.fields(row, @research, @article_database)
      end
      values
    end

    def import_bib
      values = []
      BibTeX.open(@file).each do |item|
        values << ArticleImporter::BibDatabase.fields(item, @research, @article_database)
      end
      values
    end

    def allow_database(database)
      return "ArticleImporter::#{database}Database".constantize if allowed_database.include? database
      raise "Database '#{database}' not allowed!"
    end

    def allowed_database
      %w[ACM IEEE Springer]
    end

    def after_import(article)
      ArticleDownloader.download(article: article) if @download
      ArticleCrossrefService.crossref(article.id) unless article.doi.blank? && article.issn.blank? && article.isbn.blank?
    end
  end
end
