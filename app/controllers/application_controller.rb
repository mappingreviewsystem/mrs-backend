class ApplicationController < ActionController::API
  # include ActionController::MimeResponds
  include DeviseTokenAuth::Concerns::SetUserByToken
  include Pundit

  before_action :configure_permitted_parameters, if: :devise_controller?

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  # devise alias to have it in api namespace
  alias current_user current_api_v1_user
  alias authenticate_user! authenticate_api_v1_user!

  def set_current_research
    return if current_user.nil?
    current_research = current_user.current_research
    @research = Research.find_by(id: current_research) || current_user.researches.first
  end

  private

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
    devise_parameter_sanitizer.permit(:account_update, keys: [:name])
  end

  # TODO: create a json message
  def user_not_authorized
    # flash[:alert] = 'You are not authorized to perform this action.'
    redirect_to(request.referrer || root_path)
  end

  def after_successful_token_authentication
    # Make the authentication token to be disposable
    renew_authentication_token!
  end
end
