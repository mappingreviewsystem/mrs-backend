class Api::V1::ReferenceGraphsController < Api::V1::ProtectedController
  def index
    generate_graph

    nodes = ActiveModelSerializers::SerializableResource.new(@nodes, each_serializer: NodeSerializer).as_json
    edges = ActiveModelSerializers::SerializableResource.new(@edges, each_serializer: EdgeSerializer, includes: [:source, :destination]).as_json
    render json: { nodes: nodes, edges: edges }
  end

  def show
    generate_graph(params[:id])

    nodes = ActiveModelSerializers::SerializableResource.new(@nodes, each_serializer: NodeSerializer).as_json
    edges = ActiveModelSerializers::SerializableResource.new(@edges, each_serializer: EdgeSerializer, includes: [:source, :destination]).as_json
    render json: { nodes: nodes, edges: edges }
  end

  private

  def generate_graph(article_id = nil)
    if article_id.nil?
      @nodes = @research.articles.includes(:authors).where('(articles.status = ? OR articles.status = ?) OR articles.referenced_by_count > 0', Article.statuses['extracted'], Article.statuses['selected'])
    else
      article = Article.find article_id
      @nodes = article.references
    end

    node_ids = @nodes.pluck(:id).uniq
    @edges = ArticleReference.where('source_id IN (?) AND destination_id IN (?)', node_ids, node_ids)
  end
end
