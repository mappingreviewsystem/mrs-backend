class Api::V1::ResearchesController < Api::V1::ProtectedController
  before_action :set_research, only: [:show, :update, :destroy, :statistics]

  # GET /researches
  # GET /researches.json
  def index
    @researches = ResearchPolicy::Scope.new(current_user, Research).resolve
    render json: @researches, includes: [:references_association, :tags]
  end

  # GET /researches/1
  # GET /researches/1.json
  def show
    render json: @research_model, includes: [:references_association, :tags]
  end

  # POST /researches
  # POST /researches.json
  def create
    @research_model = Research.new(research_params)

    # TODO: authorize with pundit
    @research_model.user = current_user if @research_model.user.nil?

    if @research_model.save
      render json: @research_model, status: :created
    else
      render json: @research_model.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /researches/1
  # PATCH/PUT /researches/1.json
  def update
    if @research_model.update(research_params)
      render json: @research_model, status: :ok
    else
      render json: @research_model.errors, status: :unprocessable_entity
    end
  end

  # DELETE /researches/1
  # DELETE /researches/1.json
  def destroy
    @research_model.destroy
    head :no_content
  end

  # GET /researches/1/statistics
  def statistics
    render json: @research_model, serializer: ResearchStatisticsSerializer
  end

  # GET /researches/1/spreadsheet
  def spreadsheet
    spreadsheet = ResearchSpreadsheet.generate(@research)

    begin
      name = sanitize_filename("#{@research.title}_#{Time.now.getutc}")
      filename = "#{name}.xls"
      path = "/tmp/#{filename}"

      if spreadsheet.serialize(path)
        file = File.read(path).force_encoding('BINARY')
        file = Base64.encode64(file) if params[:base64].present?

        send_data(file, type: 'text/excel', filename: filename)
      end
    ensure
      path ||= ''
      File.delete(path) if File.exist?(path)
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_research
    @research_model = Research.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def research_params
    ActiveModelSerializers::Deserialization.jsonapi_parse(params, only: [:title, :objective, :group, :keywords, :article_databases])
  end

  def sanitize_filename(filename)
    # Bad as defined by wikipedia: https://en.wikipedia.org/wiki/Filename#Reserved_characters_and_words
    # Also have to escape the backslash
    bad_chars = ['/', '\\', '?', '%', '*', ':', '|', '"', '<', '>', '.', ' ']
    bad_chars.each { |bad_char| filename.gsub!(bad_char, '_') }
    filename
  end
end
