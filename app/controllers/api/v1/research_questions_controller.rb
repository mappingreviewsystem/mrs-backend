class Api::V1::ResearchQuestionsController < Api::V1::ProtectedController
  before_action :set_research_question, only: [:show, :update, :destroy]

  # GET /research_questions
  # GET /research_questions.json
  def index
    @research_questions = ResearchQuestionPolicy::Scope.new(current_user, @research, ResearchQuestion).resolve
    render json: @research_questions
  end

  # GET /research_questions/1
  # GET /research_questions/1.json
  def show
    render json: @research_question
  end

  # POST /research_questions
  # POST /research_questions.json
  def create
    @research_question = ResearchQuestion.new(research_question_params)
    @research_question.research = @research unless @research_question.research
    @research_question.value = '' if @research_question.value.nil?

    if @research_question.save
      render json: @research_question, status: :created
    else
      render json: @research_question.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /research_questions/1
  # PATCH/PUT /research_questions/1.json
  def update
    if @research_question.update(research_question_params)
      render json: @research_question, status: :ok
    else
      render json: @research_question.errors, status: :unprocessable_entity
    end
  end

  # DELETE /research_questions/1
  # DELETE /research_questions/1.json
  def destroy
    @research_question.destroy
    head :no_content
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_research_question
    @research_question = ResearchQuestion.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def research_question_params
    ActiveModelSerializers::Deserialization.jsonapi_parse(params, only: [:title, :description, :related_metadata, :research, :value, :extraction_questions])
  end
end
