class Api::V1::ArticleExtractionQuestionsController < Api::V1::ProtectedController
  before_action :set_article_extraction_question, only: [:show, :update, :destroy]

  # GET /article_extraction_questions
  # GET /article_extraction_questions.json
  def index
    @article_extraction_questions = ArticleExtractionQuestionPolicy::Scope.new(current_user, @research, ArticleExtractionQuestion).resolve
    render json: @article_extraction_questions
  end

  # GET /article_extraction_questions/1
  # GET /article_extraction_questions/1.json
  def show
    render json: @article_extraction_question
  end

  # POST /article_extraction_questions
  # POST /article_extraction_questions.json
  def create
    @article_extraction_question = ArticleExtractionQuestion.new(article_extraction_question_params)

    if @article_extraction_question.save
      render json: @article_extraction_question, status: :created
    else
      render json: @article_extraction_question.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /article_extraction_questions/1
  # PATCH/PUT /article_extraction_questions/1.json
  def update
    if @article_extraction_question.update(article_extraction_question_params)
      render json: @article_extraction_question, status: :ok
    else
      render json: @article_extraction_question.errors, status: :unprocessable_entity
    end
  end

  # DELETE /article_extraction_questions/1
  # DELETE /article_extraction_questions/1.json
  def destroy
    @article_extraction_question.destroy
    head :no_content
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_article_extraction_question
    @article_extraction_question = ArticleExtractionQuestion.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def article_extraction_question_params
    ActiveModelSerializers::Deserialization.jsonapi_parse(params, only: [:article, :extraction_question, :value, :extraction_question_option, :extraction_question_options, :extraction_question_article_extraction_questions])
  end
end
