class Api::V1::ArticleExclusionCriteriaController < Api::V1::ProtectedController
  before_action :set_article_exclusion_criterium, only: [:show, :update, :destroy]

  # GET /article_exclusion_criteria
  # GET /article_exclusion_criteria.json
  def index
    @article_exclusion_criteria = ArticleExclusionCriterium.all
    render json: @article_exclusion_criteria
  end

  # GET /article_exclusion_criteria/1
  # GET /article_exclusion_criteria/1.json
  def show
    render json: @article_exclusion_criterium
  end

  # POST /article_exclusion_criteria
  # POST /article_exclusion_criteria.json
  def create
    @article_exclusion_criterium = ArticleExclusionCriterium.new(article_exclusion_criterium_params)

    if @article_exclusion_criterium.save
      render json: @article_exclusion_criterium, status: :created
    else
      render json: @article_exclusion_criterium.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /article_exclusion_criteria/1
  # PATCH/PUT /article_exclusion_criteria/1.json
  def update
      if @article_exclusion_criterium.update(article_exclusion_criterium_params)
        render json: @article_exclusion_criterium, status: :ok
      else
        render json: @article_exclusion_criterium.errors, status: :unprocessable_entity
      end
  end

  # DELETE /article_exclusion_criteria/1
  # DELETE /article_exclusion_criteria/1.json
  def destroy
    @article_exclusion_criterium.destroy
    head :no_content
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_article_exclusion_criterium
      @article_exclusion_criterium = ArticleExclusionCriterium.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def article_exclusion_criterium_params
      ActiveModelSerializers::Deserialization.jsonapi_parse(params, only: [:article, :exclusion_criterium])
    end
end
