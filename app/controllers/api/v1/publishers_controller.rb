class Api::V1::PublishersController < Api::V1::ProtectedController
  before_action :set_publisher, only: [:show, :update, :destroy]

  # GET /publishers
  # GET /publishers.json
  def index
    @publishers = Publisher.all
    render json: @publishers
  end

  # GET /publishers/1
  # GET /publishers/1.json
  def show
    render json: @publisher
  end

  # POST /publishers
  # POST /publishers.json
  def create
    @publisher = Publisher.new(publisher_params)
    if @publisher.save
      render json: @publisher, status: :created
    else
      render json: @publisher.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /publishers/1
  # PATCH/PUT /publishers/1.json
  def update
    if @publisher.update(publisher_params)
      render json: @publisher, status: :ok
    else
      render json: @publisher.errors, status: :unprocessable_entity
    end
  end

  # DELETE /publishers/1
  # DELETE /publishers/1.json
  def destroy
    @publisher.destroy
    head :no_content
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_publisher
    @publisher = Publisher.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def publisher_params
    ActiveModelSerializers::Deserialization.jsonapi_parse(params, only: [:name])
  end
end
