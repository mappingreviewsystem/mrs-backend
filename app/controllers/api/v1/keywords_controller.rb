class Api::V1::KeywordsController < Api::V1::ProtectedController
  before_action :set_keyword, only: [:show, :edit, :update, :destroy]

  # GET /keywords
  # GET /keywords.json
  def index
    if params[:filter]
      @keywords = Keyword.where(title: params[:filter][:title])
    else
      @keywords = Keyword.all
    end

    render json: @keywords.includes(:articles, :researches)
  end

  # GET /keywords/1
  # GET /keywords/1.json
  def show
    render json: @keyword
  end

  # GET /keywords/new
  def new
    @keyword = Keyword.new
  end

  # GET /keywords/1/edit
  def edit
  end

  # POST /keywords
  # POST /keywords.json
  def create
    @keyword = Keyword.new(keyword_params)

    if @keyword.save
      render json: @keyword, status: :created
    else
      render json: @keyword.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /keywords/1
  # PATCH/PUT /keywords/1.json
  def update
    if @keyword.update(keyword_params)
      render json: @keyword, status: :ok
    else
      render json: @keyword.errors, status: :unprocessable_entity
    end
  end

  # DELETE /keywords/1
  # DELETE /keywords/1.json
  def destroy
    @keyword.destroy

    head :no_content
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_keyword
    @keyword = Keyword.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def keyword_params
    ActiveModelSerializers::Deserialization.jsonapi_parse(params, only: [:title])
  end
end
