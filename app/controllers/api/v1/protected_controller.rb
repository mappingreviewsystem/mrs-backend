class Api::V1::ProtectedController < ApplicationController
  # before_action :authenticate_user!
  before_action :authenticate_user!
  before_action :set_current_research
end
