class Api::V1::ExtractionQuestionOptionsController < Api::V1::ProtectedController
  before_action :set_extraction_question_option, only: [:show, :update, :destroy]

  # GET /extraction_question_options
  def index
    @extraction_question_options = ExtractionQuestionOption.all

    render json: @extraction_question_options
  end

  # GET /extraction_question_options/1
  def show
    render json: @extraction_question_option
  end

  # POST /extraction_question_options
  def create
    @extraction_question_option = ExtractionQuestionOption.new(extraction_question_option_params)

    if @extraction_question_option.save
      render json: @extraction_question_option, status: :created
    else
      render json: @extraction_question_option.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /extraction_question_options/1
  def update
    if @extraction_question_option.update(extraction_question_option_params)
      render json: @extraction_question_option, status: :ok
    else
      render json: @extraction_question_option.errors, status: :unprocessable_entity
    end
  end

  # DELETE /extraction_question_options/1
  def destroy
    @extraction_question_option.destroy
    head :no_content
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_extraction_question_option
    @extraction_question_option = ExtractionQuestionOption.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def extraction_question_option_params
    ActiveModelSerializers::Deserialization.jsonapi_parse(params, only: [:extraction_question, :title, :extraction_question_article_extraction_questions])
  end
end
