class Api::V1::ArticleDatabasesController < Api::V1::ProtectedController
  before_action :set_article_database, only: [:show, :update, :destroy, :import, :download]

  # GET /article_databases
  # GET /article_databases.json
  def index
    @article_databases = ArticleDatabase.all
    render json: @article_databases
  end

  # GET /article_databases/1
  # GET /article_databases/1.json
  def show
    render json: @article_database
  end

  # POST /article_databases
  # POST /article_databases.json
  def create
    @article_database = ArticleDatabase.new(article_database_params)
    @article_database.research = @research if @article_database.research.nil?

    if @article_database.save
      render json: @article_database, status: :created
    else
      render json: @article_database.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /article_databases/1
  # PATCH/PUT /article_databases/1.json
  def update
    if @article_database.update(article_database_params)
      @article_database.exported_search.destroy if article_database_params[:exported_search] == ""
      render json: @article_database, status: :ok
    else
      render json: @article_database.errors, status: :unprocessable_entity
    end
  end

  # DELETE /article_databases/1
  # DELETE /article_databases/1.json
  def destroy
    @article_database.destroy
    head :no_content
  end

  def import
    @article_database.verify_import

    download = params[:download] == 'true'
    references = params[:references] == 'true'
    ArticleImporter.import(article_database: @article_database, download: download, references: references)
    @article_database.update(status: :importing) if @article_database.exported_search.file?

    render json: @article_database, status: :ok
  end

  def download
    respond_to do |format|
      # if @article_database.imported? && !@article_database.downloaded
      #   ArticleImporter.download(article_database: @article_database)
      #   @article_database.update(downloaded: true)
      #   format.json { render status: :ok }
      # else
        format.json { render status: :unprocessable_entity }
      # end
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_article_database
    @article_database = ArticleDatabase.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def article_database_params
    ActiveModelSerializers::Deserialization.jsonapi_parse(params, only: [:title, :url, :query, :research, :exported_search])
  end
end
