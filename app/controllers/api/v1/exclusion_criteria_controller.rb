class Api::V1::ExclusionCriteriaController < Api::V1::ProtectedController
  before_action :set_exclusion_criterium, only: [:show, :update, :destroy]

  # GET /exclusion_criteria
  # GET /exclusion_criteria.json
  def index
    @exclusion_criteria = ExclusionCriterium.all
  end

  # GET /exclusion_criteria/1
  # GET /exclusion_criteria/1.json
  def show
    render json: @exclusion_criterium
  end

  # POST /exclusion_criteria
  # POST /exclusion_criteria.json
  def create
    @exclusion_criterium = ExclusionCriterium.new(exclusion_criterium_params)
    @exclusion_criterium.research = @research

    if @exclusion_criterium.save
      render json: @exclusion_criterium, status: :created
    else
      render json: @exclusion_criterium.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /exclusion_criteria/1
  # PATCH/PUT /exclusion_criteria/1.json
  def update
    if @exclusion_criterium.update(exclusion_criterium_params)
      render json: @exclusion_criterium, status: :ok
    else
      render json: @exclusion_criterium.errors, status: :unprocessable_entity
    end
  end

  # DELETE /exclusion_criteria/1
  # DELETE /exclusion_criteria/1.json
  def destroy
    @exclusion_criterium.destroy
    head :no_content
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_exclusion_criterium
    @exclusion_criterium = ExclusionCriterium.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def exclusion_criterium_params
    ActiveModelSerializers::Deserialization.jsonapi_parse(params, only: [:title, :research])
  end
end
