class Api::V1::GroupsController < Api::V1::ProtectedController
  before_action :set_group, only: [:show, :update, :destroy]

  # GET /groups
  # GET /groups.json
  def index
    @groups = GroupPolicy::Scope.new(current_user, Group.includes(:users)).resolve
    render json: @groups
  end

  # GET /groups/1
  # GET /groups/1.json
  def show
    render json: @group
  end

  # POST /groups
  # POST /groups.json
  def create
    @group = Group.new(group_params)

    if @group.save
      @group.add_owner(current_user) if current_user.present?
      render json: @group, status: :created
    else
      render json: @group.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /groups/1
  # PATCH/PUT /groups/1.json
  def update
    if @group.update(group_params)
      render json: @group, status: :ok
    else
      render json: @group.errors, status: :unprocessable_entity
    end
  end

  # DELETE /groups/1
  # DELETE /groups/1.json
  def destroy
    @group.destroy
    head :no_content
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_group
    @group = Group.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def group_params
    ActiveModelSerializers::Deserialization.jsonapi_parse(params, only: [:name, :description, :users])
  end

  # TODO: fix this for api mode
  def add_members
    members = params[:group][:group_memberships]
    return if members.blank?

    members.each { |u| @group.add_member(User.find(u.to_i)) }
  end
end
