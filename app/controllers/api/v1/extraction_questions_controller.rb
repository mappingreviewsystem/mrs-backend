class Api::V1::ExtractionQuestionsController < Api::V1::ProtectedController
  before_action :set_extraction_question, only: [:show, :update, :destroy]

  # GET /extraction_questions
  # GET /extraction_questions.json
  def index
    @extraction_questions = ExtractionQuestionPolicy::Scope.new(current_user, @research, ExtractionQuestion).resolve
    render json: @extraction_questions
  end

  # GET /extraction_questions/1
  # GET /extraction_questions/1.json
  def show
    render json: @extraction_question
  end

  # POST /extraction_questions
  # POST /extraction_questions.json
  def create
    @extraction_question = ExtractionQuestion.new(extraction_question_params)
    @extraction_question.research = @research

    if @extraction_question.save
      render json: @extraction_question, status: :created
    else
      render json: @extraction_question.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /extraction_questions/1
  # PATCH/PUT /extraction_questions/1.json
  def update
    if @extraction_question.update(extraction_question_params)
      render json: @extraction_question, status: :ok
    else
      render json: @extraction_question.errors, status: :unprocessable_entity
    end
  end

  # DELETE /extraction_questions/1
  # DELETE /extraction_questions/1.json
  def destroy
    @extraction_question.destroy
    head :no_content
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_extraction_question
    @extraction_question = ExtractionQuestion.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def extraction_question_params
    ActiveModelSerializers::Deserialization.jsonapi_parse(params, only: [:key, :value, :research, :field_type, :range_min, :range_max, :range_increment, :research_questions, :extraction_question_options])
  end
end
