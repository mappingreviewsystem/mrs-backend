class Api::V1::ArticleReferencesController < Api::V1::ProtectedController
  before_action :set_article_reference, only: [:show, :edit, :update, :destroy]

  # GET /article_references
  # GET /article_references.json
  def index
    @article_references = ArticleReference.includes(:source, :destination)

    render json: @article_references.where(filter_references)
  end

  # GET /article_references/1
  # GET /article_references/1.json
  def show
    render json: @article_reference
  end

  # GET /article_references/new
  def new
    @article_reference = ArticleReference.new
    @articles = @research.articles.includes(:authors)
    @source_id = params[:source_id] unless params[:source_id].nil?

    render layout: false
  end

  # GET /article_references/1/edit
  def edit
  end

  # POST /article_references
  # POST /article_references.json
  def create
    @article_reference = ArticleReference.new(article_reference_params)

    respond_to do |format|
      if @article_reference.save
        format.json { render :show, status: :created, location: @article_reference }
        format.html { redirect_back(fallback_location: article_references_path, notice: 'Article Reference was successfully created.') }
      else
        format.json { render json: @article_reference.errors, status: :unprocessable_entity }
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /article_references/1
  # PATCH/PUT /article_references/1.json
  def update
    respond_to do |format|
      if @article_reference.update(article_reference_params)
        format.html { redirect_to @article_reference, notice: 'Article Reference was successfully updated.' }
        format.json { render :show, status: :ok, location: @article_reference }
      else
        format.html { render :edit }
        format.json { render json: @article_reference.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /article_references/1
  # DELETE /article_references/1.json
  def destroy
    @article_reference.destroy
    respond_to do |format|
      format.html { redirect_to article_references_url, notice: 'Article Reference was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_article_reference
    @article_reference = ArticleReference.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def article_reference_params
    ActiveModelSerializers::Deserialization.jsonapi_parse(params, only: [:source, :destination])
  end

  def filter_references
    return {} if params[:filter].blank?

    where = {}
    where[:destination_id] = params[:filter][:destination][:id] unless params[:filter][:destination].blank?
    where[:source_id] = params[:filter][:source][:id] unless params[:filter][:source].blank?
    where
  end
end
