class Api::V1::NoteTagsController < Api::V1::ProtectedController
  before_action :set_note_tag, only: [:show, :update, :destroy]

  # GET /note_tags
  def index
    @note_tags = NoteTag.all

    render json: @note_tags
  end

  # GET /note_tags/1
  def show
    render json: @note_tag
  end

  # POST /note_tags
  def create
    @note_tag = NoteTag.new(note_tag_params)

    if @note_tag.save
      render json: @note_tag, status: :created, location: @note_tag
    else
      render json: @note_tag.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /note_tags/1
  def update
    if @note_tag.update(note_tag_params)
      render json: @note_tag
    else
      render json: @note_tag.errors, status: :unprocessable_entity
    end
  end

  # DELETE /note_tags/1
  def destroy
    @note_tag.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_note_tag
    @note_tag = NoteTag.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def note_tag_params
    ActiveModelSerializers::Deserialization.jsonapi_parse(params, only: [:note, :tag])
  end
end
