class Api::V1::InclusionCriteriaController < Api::V1::ProtectedController
  before_action :set_inclusion_criterium, only: [:show, :update, :destroy]

  # GET /inclusion_criteria
  # GET /inclusion_criteria.json
  def index
    @inclusion_criteria = InclusionCriterium.all
  end

  # GET /inclusion_criteria/1
  # GET /inclusion_criteria/1.json
  def show
    render json: @inclusion_criterium
  end

  # POST /inclusion_criteria
  # POST /inclusion_criteria.json
  def create
    @inclusion_criterium = InclusionCriterium.new(inclusion_criterium_params)
    @inclusion_criterium.research = @research

    if @inclusion_criterium.save
      render json: @inclusion_criterium, status: :created
    else
      render json: @inclusion_criterium.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /inclusion_criteria/1
  # PATCH/PUT /inclusion_criteria/1.json
  def update
    if @inclusion_criterium.update(inclusion_criterium_params)
      render json: @inclusion_criterium, status: :ok
    else
      render json: @inclusion_criterium.errors, status: :unprocessable_entity
    end
  end

  # DELETE /inclusion_criteria/1
  # DELETE /inclusion_criteria/1.json
  def destroy
    @inclusion_criterium.destroy
    head :no_content
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_inclusion_criterium
    @inclusion_criterium = InclusionCriterium.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def inclusion_criterium_params
    ActiveModelSerializers::Deserialization.jsonapi_parse(params, only: [:title, :research])
  end
end
