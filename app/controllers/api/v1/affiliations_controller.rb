class Api::V1::AffiliationsController < Api::V1::ProtectedController
  before_action :set_affiliation, only: [:show, :update, :destroy]

  # GET /affiliations
  def index
    if params[:filter]
      # @affiliations = Affiliation.where('LOWER(name) LIKE LOWER(?)', "#{params[:filter][:name]}%")
      @affiliations = Affiliation.search(
        params[:filter][:name] || '*',
        fields: [:name, :location],
        includes: [:authors],
        where: { research_id: @research.id }
      )
    else
      @affiliations = Affiliation.all
    end

    render json: @affiliations
  end

  # GET /affiliations/1
  def show
    render json: @affiliation
  end

  # POST /affiliations
  def create
    @affiliation = Affiliation.new(affiliation_params)
    @affiliation.research = @research if @affiliation.research.nil?

    if @affiliation.save
      render json: @affiliation, status: :created
    else
      render json: @affiliation.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /affiliations/1
  def update
    if @affiliation.update(affiliation_params)
      render json: @affiliation
    else
      render json: @affiliation.errors, status: :unprocessable_entity
    end
  end

  # DELETE /affiliations/1
  def destroy
    @affiliation.destroy
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_affiliation
    @affiliation = Affiliation.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def affiliation_params
    ActiveModelSerializers::Deserialization.jsonapi_parse(params, only: [:name, :location, :authors, :research])
  end
end
