class Api::V1::ArticleInclusionCriteriaController < Api::V1::ProtectedController
  before_action :set_article_inclusion_criterium, only: [:show, :update, :destroy]

  # GET /article_inclusion_criteria
  # GET /article_inclusion_criteria.json
  def index
    @article_inclusion_criteria = ArticleInclusionCriterium.all
    render json: @article_inclusion_criteria
  end

  # GET /article_inclusion_criteria/1
  # GET /article_inclusion_criteria/1.json
  def show
    render json: @article_inclusion_criterium
  end

  # POST /article_inclusion_criteria
  # POST /article_inclusion_criteria.json
  def create
    @article_inclusion_criterium = ArticleInclusionCriterium.new(article_inclusion_criterium_params)

    if @article_inclusion_criterium.save
      render json: @article_inclusion_criterium, status: :created
    else
      render json: @article_inclusion_criterium.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /article_inclusion_criteria/1
  # PATCH/PUT /article_inclusion_criteria/1.json
  def update
    if @article_inclusion_criterium.update(article_inclusion_criterium_params)
      render json: @article_inclusion_criterium, status: :ok
    else
      render json: @article_inclusion_criterium.errors, status: :unprocessable_entity
    end
  end

  # DELETE /article_inclusion_criteria/1
  # DELETE /article_inclusion_criteria/1.json
  def destroy
    @article_inclusion_criterium.destroy
    head :no_content
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_article_inclusion_criterium
    @article_inclusion_criterium = ArticleInclusionCriterium.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def article_inclusion_criterium_params
    ActiveModelSerializers::Deserialization.jsonapi_parse(params, only: [:article, :inclusion_criterium])
  end
end
