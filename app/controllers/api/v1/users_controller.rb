class Api::V1::UsersController < Api::V1::ProtectedController
  skip_before_action :authenticate_user!, only: :create
  skip_before_action :set_current_research, only: :create
  before_action :set_user, except: [:index, :create]

  def index
    if params[:filter]
      # @users = User.where('LOWER(name) LIKE LOWER(?)', "#{params[:filter][:name]}%")
      @users = User.search(
        params[:filter][:name] || '*',
        fields: [:name, :email]
      )
    else
      @users = User.all
    end

    render json: @users
  end

  # GET /users/1
  # GET /users/1.json
  def show
    serializer = @user == current_user ? UserMeSerializer : UserSerializer
    render json: @user, serializer: serializer
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    if @user.save
      render json: @user, status: :created
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    if @user.update(user_params)
      render json: @user, status: :ok
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    head :ok
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    ActiveModelSerializers::Deserialization.jsonapi_parse(params, only: [:name, :current_research, :email, :password, :password_confirmation])
  end
end
