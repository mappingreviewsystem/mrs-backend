class Api::V1::FundersController < Api::V1::ProtectedController
  before_action :set_funder, only: [:show, :update, :destroy]

  # GET /funders
  def index
    @funders = Funder.all

    render json: @funders
  end

  # GET /funders/1
  def show
    render json: @funder
  end

  # POST /funders
  def create
    @funder = Funder.new(funder_params)

    if @funder.save
      render json: @funder, status: :created
    else
      render json: @funder.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /funders/1
  def update
    if @funder.update(funder_params)
      render json: @funder
    else
      render json: @funder.errors, status: :unprocessable_entity
    end
  end

  # DELETE /funders/1
  def destroy
    @funder.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_funder
      @funder = Funder.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def funder_params
      ActiveModelSerializers::Deserialization.jsonapi_parse(params, only: [:name, :url, :location, :crossref, :doi])
    end
end
