class Api::V1::AuthorsController < Api::V1::ProtectedController
  before_action :set_author, only: [:show, :update, :destroy]

  # GET /authors
  # GET /authors.json
  def index
    if params[:filter]
      # @authors = Author.where('LOWER(name) LIKE LOWER(?)', "%#{params[:filter][:name]}%")
      @authors = Author.search(
        params[:filter][:name] || '*',
        fields: [:name, :affiliations],
        includes: [:affiliations, :articles],
        where: { research_id: @research.id }
      )
    else
      @authors = Author.all.includes(:articles, :affiliations)
    end

    render json: @authors
  end

  # GET /authors/1
  # GET /authors/1.json
  def show
    render json: @author
  end

  # POST /authors
  # POST /authors.json
  def create
    @author = Author.new(author_params)
    @author.research = @research if @author.research.nil?

    if @author.save
      render json: @author, status: :created
    else
      render json: @author.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /authors/1
  # PATCH/PUT /authors/1.json
  def update
    if @author.update(author_params)
      render json: @author, status: :ok
    else
      render json: @author.errors, status: :unprocessable_entity
    end
  end

  # DELETE /authors/1
  # DELETE /authors/1.json
  def destroy
    @author.destroy

    head :no_content
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_author
    @author = Author.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def author_params
    ActiveModelSerializers::Deserialization.jsonapi_parse(params, only: [:name, :affiliations, :research])
  end
end
