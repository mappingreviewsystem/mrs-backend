class Api::V1::ArticlesController < Api::V1::ProtectedController
  before_action :set_article, only: [:show, :update, :destroy, :crossref]

  # GET /articles
  # GET /articles.json
  def index
    if params['search'].nil?
      @articles = ArticlePolicy::Scope.new(current_user, @research, Article).resolve
      render json: @articles.where(where_params).includes(:authors, :keywords, :tags, :notes, :publication)
    else
      search!
      render json: @articles, each_serializer: ArticleSearchSerializer, meta: @statistics
    end
  end


  # GET /articles/1
  # GET /articles/1.json
  def show
    render json: @article
  end

  # POST /articles
  # POST /articles.json
  def create
    @article = Article.new(article_params)
    @article.research = @research

    crossref = true
    crossref ||= params[:crossref]

    if @article.save
      ArticleCrossrefService.crossref(@article.id, params[:references]) if crossref && @article.doi.present?
      update_article_pages

      render json: @article, status: :created
    else
      render json: @article.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /articles/1
  # PATCH/PUT /articles/1.json
  def update
    # TODO: fix references counter when remove association
    article = false
    Article.transaction do
      references_decrement_counter_fix
      article = @article.update(article_params)
    end

    if article
      update_article_pages
      render json: @article, status: :ok
    else
      render json: @article.errors, status: :unprocessable_entity
    end
  end

  # DELETE /articles/1
  # DELETE /articles/1.json
  def destroy
    @article.destroy
    head :no_content
  end

  def crossref
    ArticleCrossrefService.crossref_sync(@article.id, params[:references])
    render json: @article, status: :ok
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_article
    @article = Article.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def article_params
    ActiveModelSerializers::Deserialization.jsonapi_parse(params, only: [:title, :content_type_id, :url, :publication_year, :doi, :volume, :issue, :page_start, :page_end, :isbn, :issn, :data, :status, :file, :publication, :authors, :keywords, :content_type, :tags, :references, :referenced_by])
  end

  def search!
    @articles_search = params['search'].blank? ? nil : params['search']

    @articles = Article.search(
      @articles_search || '*',
      fields: [:title, :authors, :keywords, :publication, :publisher],
      includes: [:publisher, :tags],
      where: filter_params,
      aggs: [:status],
      order: sort_params,
      highlight: { tag: '<u>' },
      page: params[:page],
      per_page: Article.default_per_page
    )

    search_statistics
  end

  # TODO: create a service or a better abstraction to filter this...
  def filter_params
    return { research_id: @research.id } if params['filter'].blank?

    allowed_filters = %w[status article_database_id inclusion_criterium_ids exclusion_criterium_ids extraction_question_ids tag_ids]
    where = params['filter'].delete_if do |key, value|
      !allowed_filters.include?(key) || value.blank?
    end
    where[:research_id] = @research.id

    where
  end

  def where_params
    return {} if params[:filter].blank?
    params.require(:filter).permit(title: [], status: [], tags: [])
  end

  def sort_params
    params[:sort] || { status_id: :asc, referenced_by_count: :desc, _score: :desc }
  end

  def search_statistics
    @statistics = {
      total_pages: @articles.total_pages,
      count: {
        total: @articles.aggregations['status']['doc_count']
      }
    }

    # status doc counter
    @articles.aggregations['status']['status']['buckets'].each do |status|
      @statistics[:count][status['key']] = status['doc_count']
    end
  end

  # TODO: call this in model when file really updates
  def update_article_pages
    ArticleTextExtractorWorker.perform_async(@article.id, true) unless article_params['file'].nil?
  end

  # TODO: find why references counter doesn't update when a reference is removed (inclusion is fine)
  def references_decrement_counter_fix
    # references
    reference_ids = article_params[:reference_ids] || []
    article_ids = @article.reference_ids - reference_ids.map(&:to_i)
    unless article_ids.empty?
      @article.decrement!(:references_count, article_ids.size)
      articles = Article.where(id: article_ids)
      articles.each { |a| a.decrement!(:referenced_by_count, 1) }
    end

    # referenced_by
    referenced_by_ids = article_params[:referenced_by_ids] || []
    article_ids = @article.referenced_by_ids - referenced_by_ids.map(&:to_i)
    unless article_ids.empty?
      @article.decrement!(:referenced_by_count, article_ids.size)
      articles = Article.where(id: article_ids)
      articles.each { |a| a.decrement!(:references_count, 1) }
    end
  end
end
