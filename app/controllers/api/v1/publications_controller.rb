class Api::V1::PublicationsController < Api::V1::ProtectedController
  before_action :set_publication, only: [:show, :update, :destroy]

  # GET /publications
  # GET /publications.json
  def index
    if params[:filter]
      # @publications = Publication.where('LOWER(title) LIKE LOWER(?)', "#{params[:filter][:title]}%")
      @publications = Publication.search(
        params[:filter][:title] || '*',
        fields: [:title],
        includes: [:articles, :publisher],
        where: { research_id: @research.id }
      )
    else
      @publications = Publication.all.includes(:articles, :publisher)
    end

    render json: @publications
  end

  # GET /publications/1
  # GET /publications/1.json
  def show
    render json: @publication
  end

  # POST /publications
  # POST /publications.json
  def create
    @publication = Publication.new(publication_params)
    @publication.research = @research if @publication.research.nil?

    if @publication.save
      render json: @publication, status: :created
    else
      render json: @publication.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /publications/1
  # PATCH/PUT /publications/1.json
  def update
    if @publication.update(publication_params)
      render json: @publication, status: :ok
    else
      render json: @publication.errors, status: :unprocessable_entity
    end
  end

  # DELETE /publications/1
  # DELETE /publications/1.json
  def destroy
    @publication.destroy
    head :no_content
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_publication
    @publication = Publication.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def publication_params
    ActiveModelSerializers::Deserialization.jsonapi_parse(params, only: [:title, :publisher, :research])
  end
end
