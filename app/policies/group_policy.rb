class GroupPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      case user.role
      when 'user'
        scope.from_member(user)
      when 'admin'
        scope.all
      end
    end
  end
end
