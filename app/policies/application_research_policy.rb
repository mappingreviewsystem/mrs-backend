class ApplicationResearchPolicy
  attr_reader :user, :record

  def initialize(user, research, record)
    @user = user
    @research = research
    @record = record
  end

  def index?
    false
  end

  def show?
    scope.where(id: record.id).exists?
  end

  def create?
    research.member?(user)
  end

  def new?
    create?
  end

  def update?
    research.member?(user)
  end

  def edit?
    update?
  end

  def destroy?
    research.member?(user)
  end

  def scope
    Pundit.policy_scope!(user, record.class)
  end

  class Scope
    attr_reader :user, :research, :scope

    def initialize(user, research, scope)
      @user = user
      @research = research
      @scope = scope
    end

    def resolve
      case user.role
      when 'user'
        return [] if research.user != user
        scope.where(research: research)
      when 'admin'
        return scope.where(research: research) unless research.nil?
        scope.all
      end
    end
  end
end
