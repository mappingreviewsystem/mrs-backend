class ResearchPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      case user.role
      when 'user'
        scope.from_user(user)
      when 'admin'
        scope.all
      end
    end
  end
end
