class GroupMembership < ApplicationRecord
  belongs_to :group
  belongs_to :user

  validates :user, presence: true, uniqueness: { scope: :group }
  validates :group, presence: true

  def to_s
    user
  end
end
