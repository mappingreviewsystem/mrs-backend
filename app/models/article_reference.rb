class ArticleReference < ApplicationRecord
  belongs_to :source, class_name: 'Article'
  counter_culture :source, column_name: 'references_count'
  belongs_to :destination, class_name: 'Article'
  counter_culture :destination, column_name: 'referenced_by_count'

  enum status: { regular: 0, important: 1 }

  validates :source, uniqueness: { scope: :destination }
  validate :not_self_reference

  private

  def not_self_reference
    errors.add(:destination, "can't be same as source") if source_id == destination_id
  end

  def same_research
    errors.add(:destination, "can't be from another research") if source.research_id != destination.research_id
  end
end
