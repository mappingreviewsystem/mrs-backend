class ArticleAuthor < ApplicationRecord
  include SameResearchable
  belongs_to :article
  belongs_to :author

  validates :article, uniqueness: { scope: :author }

  # full-text search
  searchkick callbacks: :async
  after_commit :reindex_associations
  def reindex_associations
    article.reindex(mode: :async)
  end

  def same_research_models
    [author, article]
  end
end
