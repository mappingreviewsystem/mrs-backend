class ArticleExclusionCriterium < ApplicationRecord
  # include Doubtable
  include SameResearchable
  include ArticleUpdateable

  belongs_to :article
  belongs_to :exclusion_criterium

  validates :article, uniqueness: { scope: :exclusion_criterium }

  def same_research_models
    [article, exclusion_criterium]
  end
end
