class Keyword < ApplicationRecord
  has_many :article_keywords, dependent: :destroy
  has_many :articles, through: :article_keywords
  has_many :research_keywords, dependent: :destroy
  has_many :researches, through: :research_keywords

  # full-text search
  searchkick callbacks: :async
  after_commit :reindex_associations
  def reindex_associations
    articles.find_each { |article| article.reindex(mode: :async) }
  end

  def to_s
    title
  end
end
