class Tag < ApplicationRecord
  include Colorable

  belongs_to :research
  has_many :article_tags, dependent: :destroy
  has_many :articles, through: :article_tags
  has_many :note_tags, dependent: :destroy
  has_many :notes, through: :note_tags

  validates :label, presence: true, uniqueness: { scope: :color }
end
