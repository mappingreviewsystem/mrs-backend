class ArticlePublication < ApplicationRecord
  include SameResearchable
  belongs_to :article
  belongs_to :publication

  # full-text search
  searchkick callbacks: :async
  after_commit :reindex_associations
  def reindex_associations
    article.reindex(mode: :async)
  end

  private

  def same_research_models
    [article, publication]
  end
end
