class Funder < ApplicationRecord
  belongs_to :research

  has_many :articles

  validates :doi, uniqueness: true, allow_nil: true
  validates :crossref_id, uniqueness: true, allow_nil: true
  validates :name, presence: true
end
