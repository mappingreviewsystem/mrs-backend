class ArticleInclusionCriterium < ApplicationRecord
  # include Doubtable
  include SameResearchable
  include ArticleUpdateable

  belongs_to :article
  belongs_to :inclusion_criterium

  validates :article, uniqueness: { scope: :inclusion_criterium }

  def same_research_models
    [article, inclusion_criterium]
  end
end
