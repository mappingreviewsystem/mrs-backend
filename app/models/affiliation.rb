class Affiliation < ApplicationRecord
  belongs_to :research

  has_many :affiliation_authors, dependent: :destroy
  has_many :authors, through: :affiliation_authors
  has_many :articles, through: :authors

  validates :name, uniqueness: { scope: [:location, :research] }
  validates :location, inclusion: { in: ISO3166::Country.codes }, allow_nil: true

  def location=(value)
    if !value.blank? && value.size > 2
      super(ISO3166::Country.find_country_by_name(value).try(:gec))
    else
      super(value)
    end
  end

  # full-text search
  after_commit :reindex_associations
  def reindex_associations
    authors.find_each { |author| author.reindex(mode: :async) }
  end
  searchkick callbacks: :async, _all: false, default_fields: [:name, :location]
  def search_data
    {
      all: [name, location].join(' '),
      name: name,
      location: location,
      research_id: research_id,
      author_ids: authors.pluck(:id)
    }
  end
  scope :search_import, -> { includes(:authors) }
end
