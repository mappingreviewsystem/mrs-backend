class Article < ApplicationRecord
  include Keywordable
  nilify_blanks

  belongs_to :article_database, optional: true
  belongs_to :funder, optional: true
  belongs_to :content_type
  belongs_to :research, counter_cache: true

  has_one :article_publication, dependent: :destroy
  has_one :publication, through: :article_publication
  has_one :publisher, through: :publication

  has_many :notes, dependent: :destroy
  has_many :article_tags, dependent: :destroy
  has_many :tags, through: :article_tags
  has_many :article_authors, dependent: :destroy
  has_many :authors, through: :article_authors
  has_many :article_keywords, dependent: :destroy
  has_many :keywords, through: :article_keywords
  has_many :article_pages, dependent: :destroy
  has_many :article_inclusion_criteria, dependent: :destroy
  has_many :inclusion_criteria, through: :article_inclusion_criteria
  has_many :article_exclusion_criteria, dependent: :destroy
  has_many :exclusion_criteria, through: :article_exclusion_criteria
  has_many :article_extraction_questions, dependent: :destroy
  has_many :extraction_questions, through: :article_extraction_questions
  has_many :extraction_question_options, through: :article_extraction_questions

  has_many :references_association, class_name: 'ArticleReference', foreign_key: 'source_id', dependent: :destroy
  has_many :references, through: :references_association, source: :destination
  has_many :inverse_references_association, class_name: 'ArticleReference', foreign_key: 'destination_id', dependent: :destroy
  has_many :referenced_by, through: :inverse_references_association, source: :source

  accepts_nested_attributes_for :authors, :keywords, :publication, :tags, :notes

  has_attached_file :file, default_url: ''
  validates_attachment :file, content_type: { content_type: 'application/pdf' }

  validates :title, presence: true
  validates :research, presence: true
  validates :research, uniqueness: { scope: :doi }, unless: proc { |article| article.doi.blank? }
  # validates :research, uniqueness: { scope: [:article_database, :doi] }, unless: proc { |article| article.doi.blank? }
  validates :research, uniqueness: { scope: :title }, if: proc { |article| article.doi.blank? }

  enum status: { extracted: 0, selected: 1, unseen: 2, excluded: 3, not_included: 4 }

  before_save :update_status

  # auto strip
  auto_strip_attributes :title, squish: true

  # kaminari
  paginates_per 10

  # full-text search
  searchkick callbacks: :async, _all: false, default_fields: [:title, :authors, :keywords, :publication, :publisher]
  def search_data
    {
      all: [title, authors, keywords, publication, publisher].join(' '),
      title: title,
      url: url,
      doi: doi,
      isbn: isbn,
      issn: issn,
      publication_year: publication_year,
      status: status,
      status_id: Article.statuses[status],
      article_database_id: article_database_id,
      research_id: research_id,
      authors: authors.pluck(:name),
      keywords: keywords.pluck(:title),
      publication: publication.try(:title),
      publisher: publisher.try(:name),
      article_pages: article_pages.pluck(:value),
      referenced_by_count: referenced_by_count,
      inclusion_criterium_ids: article_inclusion_criteria.pluck(:inclusion_criterium_id),
      exclusion_criterium_ids: article_exclusion_criteria.pluck(:exclusion_criterium_id),
      extraction_question_ids: article_extraction_questions.where.not(value: '').pluck(:extraction_question_id),
      tag_ids: tags.pluck(:id)
    }
  end
  scope :search_import, -> { includes(:authors, :keywords, :publication, :publisher, :article_pages, :article_inclusion_criteria, :article_exclusion_criteria) }

  # TODO: improve this to do a global (per research) duplicated articles
  # def duplicated_articles(research_id)
  #   Article.where('id != ? AND research_id = ? AND doi LIKE ?', id, research_id, doi)
  # end

  def first_author
    return if authors.empty?

    authors.first.name
  end

  def authors_list
    return [] if authors.blank?

    authors.map do |a|
      author = a.name
      a.affiliations.each do |affiliation|
        author += " (#{affiliation.name} - #{affiliation.location})"
      end
      author
    end
  end

  def publication_id=(id)
    self.publication = id.blank? ? nil : Publication.find(id)
  end

  def tag_list
    tags.where('priority < 999')
  end

  def tag_ribbon
    tags.where(priority: 999).first
  end

  def chosen?
    %w[extracted selected].include? status
  end

  def relevant?
    chosen? || referenced_by.count > 1
  end

  def referenced?
    if status == 'not_included'
      referenced_by.count > 1
    else
      referenced_by.count > 0
    end
  end

  # private

  def update_status
    return true if status == 'not_included'

    self.status = if !article_exclusion_criteria.empty?
                    :excluded
                  elsif !article_inclusion_criteria.empty?
                    if !research.extraction_questions.empty? && (article_extraction_questions.count == research.extraction_questions.count)
                      :extracted
                    else
                      :selected
                    end
                  else
                    :unseen
                  end
  end

  # TODO: find a callback to call this when file is really updated
  def update_article_pages
    ArticleTextExtractorWorker.perform_async(id, true)
  end
end
