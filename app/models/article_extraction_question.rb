class ArticleExtractionQuestion < ApplicationRecord
  # include Doubtable
  include SameResearchable
  include ArticleUpdateable

  belongs_to :article
  belongs_to :extraction_question
  belongs_to :extraction_question_option, optional: true
  has_many :extraction_question_option_article_extraction_questions, dependent: :destroy
  has_many :extraction_question_options, through: :extraction_question_option_article_extraction_questions

  validates :article, uniqueness: { scope: :extraction_question }
  validates :value, presence: true, if: proc { |ae| ae.extraction_question.text_type? || ae.extraction_question.range_type? }

  validate :range_min_max_values

  def range_min_max_values
    return unless extraction_question.range_type?
    return errors.add(:value, 'is smaller than minimum.') if value.to_i < extraction_question.range_min.to_i
    errors.add(:value, 'is bigger than maximum.') if value.to_i > extraction_question.range_max.to_i
  end

  # full-text search
  def reindex_associations
    article.reindex(mode: :async)
    extraction_question.reindex(mode: :async)

    return if extraction_question_option.nil?
    extraction_question_option.reindex(mode: :async)
  end

  def same_research_models
    [article, extraction_question]
  end
end
