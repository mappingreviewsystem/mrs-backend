class Author < ApplicationRecord
  belongs_to :research

  has_many :article_authors, dependent: :destroy
  has_many :articles, through: :article_authors
  has_many :affiliation_authors, dependent: :destroy
  has_many :affiliations, through: :affiliation_authors

  validates :name, presence: true

  # full-text search
  after_commit :reindex_associations
  def reindex_associations
    articles.find_each { |article| article.reindex(mode: :async) }
    affiliations.find_each { |affiliation| affiliation.reindex(mode: :async) }
  end
  searchkick callbacks: :async, _all: false, default_fields: [:name, :affiliations]
  def search_data
    {
      all: [name, affiliations.pluck(:name)].join(' '),
      name: name,
      affiliations: affiliations.pluck(:name),
      research_id: research_id,
      article_ids: articles.pluck(:id)
    }
  end
  scope :search_import, -> { includes(:affiliations, :articles) }

  def to_s
    name
  end
end
