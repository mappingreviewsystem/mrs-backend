class Crossref < ApplicationRecord
  validates :doi, uniqueness: true, allow_nil: true
  validates :issn, uniqueness: true, allow_nil: true
  validates :isbn, uniqueness: true, allow_nil: true
  validate :at_least_one_identifier

  def at_least_one_identifier
    presence = doi.present? || issn.present? || isbn.present?
    errors.add(:identifier, 'at least one identifier') unless presence
  end

  def data_hash
    JSON.parse(data)
  end

  def issn=(value)
    StdNum::ISSN.normalize(value)
  end

  def isbn=(value)
    StdNum::ISBN.normalize(value)
  end

  def self.normalize_issn(value)
    StdNum::ISSN.normalize(value)
  end

  def self.normalize_isbn(value)
    StdNum::ISBN.normalize(value)
  end
end
