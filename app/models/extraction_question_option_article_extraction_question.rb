class ExtractionQuestionOptionArticleExtractionQuestion < ApplicationRecord
  belongs_to :extraction_question_option
  belongs_to :article_extraction_question
end
