class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  class << self
    attr_accessor :graph_ql_type
  end
end
