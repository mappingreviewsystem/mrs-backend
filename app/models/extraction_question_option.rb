class ExtractionQuestionOption < ApplicationRecord
  belongs_to :extraction_question

  has_many :article_extraction_questions, dependent: :destroy
  has_many :articles, through: :article_extraction_questions

  validates :title, presence: true

  # full-text search
  searchkick callbacks: :async
  after_commit :reindex_associations
  def reindex_associations
    extraction_question.reindex(mode: :async)
  end
end
