class ResearchResearchQuestion < ApplicationRecord
  belongs_to :research
  belongs_to :research_question
end
