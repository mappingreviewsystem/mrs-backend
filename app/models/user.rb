class User < ApplicationRecord
  # acts_as_token_authenticatable

  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :trackable, :validatable
          # :confirmable, :omniauthable
  include DeviseTokenAuth::Concerns::User

  has_many :researches, -> { where group_id: nil }
  has_many :group_memberships, dependent: :destroy
  has_many :groups, through: :group_memberships
  has_many :group_researches, through: :groups, source: :researches
  belongs_to :current_research, class_name: 'Research', optional: true

  enum role: { user: 0, admin: 1 }

  validates :name, presence: true

  def to_s
    name
  end

  def change_current_research(research)
    return false unless Research.from_user(self).include? research
    update(current_research: research)
  end

  # full-text search
  searchkick callbacks: :async, _all: false, default_fields: [:name, :email]
  def search_data
    {
      all: [name, email].join(' '),
      name: name,
      email: email
    }
  end
end
