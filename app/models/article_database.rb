class ArticleDatabase < ApplicationRecord
  belongs_to :research
  has_many :articles, dependent: :destroy

  has_attached_file :exported_search
  validates_attachment :exported_search, content_type: { content_type: ['text/plain', 'text/csv'] }

  validates :title, presence: true, uniqueness: { scope: :research }

  enum status: { not_imported: 0, importing: 1, imported: 2 }

  # full-text search
  searchkick callbacks: :async

  def verify_import
    update(status: :not_imported) if updated_at > 30.minutes.ago && status == :importing
  end
end
