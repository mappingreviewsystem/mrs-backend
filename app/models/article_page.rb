class ArticlePage < ApplicationRecord
  belongs_to :article

  validates :article, presence: true, uniqueness: { scope: :page }
  validates :page, presence: true

  default_scope { order(page: :asc) }

  # full-text search
  searchkick callbacks: :async
  after_commit :reindex_associations
  def reindex_associations
    article.reindex(mode: :async)
  end
  def search_data
    {
      article_id: article_id,
      page: page,
      value: value
    }
  end
end
