class ExclusionCriterium < ApplicationRecord
  belongs_to :research
  has_many :article_exclusion_criteria, dependent: :destroy
  has_many :articles, through: :article_exclusion_criteria

  validates :title, presence: true
end
