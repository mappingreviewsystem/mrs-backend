class InclusionCriterium < ApplicationRecord
  belongs_to :research
  has_many :article_inclusion_criteria, dependent: :destroy
  has_many :articles, through: :article_inclusion_criteria

  validates :title, presence: true
end
