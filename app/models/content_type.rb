class ContentType < ApplicationRecord
  has_many :articles

  validates :code, presence: true, uniqueness: true
end
