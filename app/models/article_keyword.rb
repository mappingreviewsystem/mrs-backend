class ArticleKeyword < ApplicationRecord
  belongs_to :article
  belongs_to :keyword

  validates :article, uniqueness: { scope: :keyword }

  # full-text search
  searchkick callbacks: :async
  after_commit :reindex_associations
  def reindex_associations
    article.reindex(mode: :async)
  end
end
