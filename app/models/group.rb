class Group < ApplicationRecord
  has_many :researches
  has_many :group_memberships, dependent: :destroy
  has_many :users, through: :group_memberships
  has_many :owner_memberships, -> { where owner: true }, class_name: 'GroupMembership'
  has_many :owners, through: :owner_memberships, source: :user

  scope :from_member, ->(user) { Group.find(includes(:group_memberships).where(group_memberships: { user_id: user.id }).pluck(:id)) }

  def add_member(user)
    membership = GroupMembership.find_by(group: self, user: user)
    return membership.update(owner: false) if !membership.nil? && membership.owner
    GroupMembership.create(group: self, user: user, owner: false)
  end

  def add_owner(user)
    membership = GroupMembership.find_by(group: self, user: user)
    return membership.update(owner: true) if !membership.nil? && !membership.owner
    GroupMembership.create(group: self, user: user, owner: true)
  end

  def members
    users
  end

  def member?(user)
    users.include? user
  end

  def owner?(user)
    !group_memberships.where(user: user, owner: true).blank?
  end
end
