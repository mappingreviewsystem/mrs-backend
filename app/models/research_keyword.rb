class ResearchKeyword < ApplicationRecord
  belongs_to :research
  belongs_to :keyword

  validates :research, uniqueness: { scope: :keyword }

  # full-text search
  searchkick callbacks: :async
  after_commit :reindex_associations
  def reindex_associations
    research.reindex(mode: :async)
  end
end
