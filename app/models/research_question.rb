class ResearchQuestion < ApplicationRecord
  belongs_to :research
  has_and_belongs_to_many :extraction_questions

  def to_s
    "#{title}: #{description}"
  end
end
