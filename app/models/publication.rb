class Publication < ApplicationRecord
  belongs_to :publisher, optional: true
  belongs_to :research

  has_many :article_publications, dependent: :destroy
  has_many :articles, through: :article_publications

  validates :title, presence: true, uniqueness: { scope: [:publisher, :research] }
  validates :issn, uniqueness: { scope: :research }, unless: proc { |publication| publication.issn.blank? }
  validates :isbn, uniqueness: { scope: :research }, unless: proc { |publication| publication.isbn.blank? }

  # auto strip
  auto_strip_attributes :title, squish: true

  searchkick callbacks: :async, _all: false, default_fields: [:title, :location]
  def search_data
    {
      all: [title, location].join(' '),
      title: title,
      location: location,
      issn: issn,
      isbn: isbn,
      research_id: research_id,
      publication_type: publication_type
    }
  end
end
