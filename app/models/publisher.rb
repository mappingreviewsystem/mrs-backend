class Publisher < ApplicationRecord
  has_many :publications
  has_many :articles, through: :publications

  def to_s
    name
  end
end
