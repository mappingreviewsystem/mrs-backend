class ExtractionQuestion < ApplicationRecord
  belongs_to :research
  has_and_belongs_to_many :research_questions
  has_many :article_extraction_questions, dependent: :destroy
  has_many :articles, through: :article_extraction_questions
  has_many :extraction_question_options, dependent: :destroy

  enum field_type: { text: 0, range: 1, range_radio: 2, radiobox: 3, selector: 4, checkbox: 5 }

  validates :field_type, presence: true
  validates :key, presence: true

  # types of value
  def text_type?
    text?
  end

  def range_type?
    range? || range_radio?
  end

  def single_type?
    radiobox? || selector?
  end

  def multi_type?
    checkbox?
  end

  # full-text search
  searchkick callbacks: :async
  def search_data
    option_values = article_extraction_questions.where.not(extraction_question_option_id: nil).pluck(:id)
    {
      research_id: research.id,
      key: key,
      value: value,
      values: article_extraction_questions.where.not(value: '').pluck(:value),
      option_values: option_values
    }
  end
  scope :search_import, -> { includes(:article_extraction_questions) }
end
