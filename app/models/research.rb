class Research < ApplicationRecord
  include Keywordable

  belongs_to :user
  belongs_to :group, optional: true

  # has_one :current_user, class_name: 'User', foreign_key: :current_research_id

  has_many :research_keywords, dependent: :destroy
  has_many :keywords, through: :research_keywords
  has_many :articles, dependent: :destroy
  has_many :funders, dependent: :destroy
  has_many :publications, dependent: :destroy
  has_many :authors, dependent: :destroy
  has_many :affiliations, dependent: :destroy
  has_many :article_references, through: :articles, source: :references_association
  has_many :selected_articles, -> { where status: ['extracted', 'selected'] }, class_name: 'Article'
  has_many :selected_publications, through: :selected_articles, class_name: 'Publication'
  has_many :tags, dependent: :destroy
  has_many :notes, dependent: :destroy
  has_many :article_databases, dependent: :destroy
  # has_many :research_research_questions, dependent: :destroy
  has_many :research_questions, dependent: :destroy
  has_many :extraction_questions, dependent: :destroy
  has_many :inclusion_criteria, dependent: :destroy
  has_many :exclusion_criteria, dependent: :destroy
  has_many :selected_articles, -> { where 'status = ? OR status = ?', Article.statuses['extracted'], Article.statuses['selected'] }, class_name: 'Article'
  has_many :excluded_articles, -> { where 'status = ? OR status = ?', Article.statuses['excluded'], Article.statuses['unseen'] }, class_name: 'Article'

  scope :from_user, ->(user) { where('user_id = ? OR group_id IN (?)', user.id, user.groups.pluck(:id)) }

  validates :title, presence: true

  def member?(u)
    user == u || group.member?(u)
  end

  def owner?(u)
    user == u || group.owner?(u)
  end

  # full-text search
  searchkick callbacks: :async
end
