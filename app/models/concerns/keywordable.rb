module Keywordable
  extend ActiveSupport::Concern

  def keyword_list
    keywords.map(&:title)
  end

  def keyword_list=(titles)
    self.keywords = titles.uniq.map do |t|
      Keyword.where(title: t.strip).first_or_create!
    end
  end
end
