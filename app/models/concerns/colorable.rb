module Colorable
  extend ActiveSupport::Concern

  included do
    # validates :color, inclusion: { in: colors }
  end

  class_methods do
    def colors
      %w[red orange yellow olive green teal blue violet purple pink brown grey black]
    end
  end
end
