module ArticleUpdateable
  extend ActiveSupport::Concern

  included do
    after_save :update_article
    after_destroy :update_article

    # full-text search
    searchkick callbacks: :async
    after_commit :reindex_associations
  end

  def update_article
    article.update_attribute(:updated_at, Time.now)
  end

  # full-text search
  def reindex_associations
    article.reindex(mode: :async)
  end
end
