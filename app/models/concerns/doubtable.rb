module Doubtable
  extend ActiveSupport::Concern

  included do
    enum doubt: { no: 0, yes: 1 }
  end
end
