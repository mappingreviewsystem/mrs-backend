module SameResearchable
  extend ActiveSupport::Concern

  included do
    validate :same_research_validation
  end

  def same_research_validation
    errors.add(same_research_models.last.class.name.tableize, "can't be from another research") if same_research_models.first.research_id != same_research_models.last.research_id
  end

  def same_research_models
    raise 'Must implement \'same_research_models\' for \'SameResearchable\' concern'
  end
end
