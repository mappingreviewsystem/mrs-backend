class AffiliationAuthor < ApplicationRecord
  include SameResearchable

  belongs_to :affiliation
  belongs_to :author

  validates :author, presence: true, uniqueness: { scope: [:affiliation] }

  private

  def same_research_models
    [author, affiliation]
  end
end
