class Note < ApplicationRecord
  include Colorable

  belongs_to :article, optional: true
  belongs_to :research, optional: true

  has_many :note_tags, dependent: :destroy
  has_many :tags, through: :note_tags

  validate :one_association

  private

  def one_association
    errors.add(:article, 'need to have an article or research reference') unless article_id.present? ^ research_id.present?
  end
end
