class ArticleTextExtractorWorker
  include Sidekiq::Worker

  def perform(article_id, override = false)
    return true if article_id.blank?
    find_article(article_id)
    return true unless @article.file.exists?

    @output_path = "/tmp/article_pages/#{@article.id}/"
    extract_text_from_document_file

    create_article_pages(override)

    true
  ensure
    # clean temporary page files
    FileUtils.rm_rf(@output_path) unless @output_path.nil? || @output_path == '/'
  end

  private

  def find_article(article_id)
    @article = Article.find article_id
    raise "Article \##{article_id} not found!" if @article.nil?
  end

  def extract_text_from_document_file
    Docsplit.extract_text(@article.file.path, pages: 'all', output: @output_path)
  end

  def create_article_pages(override)
    @filename = File.basename(@article.file.path, File.extname(@article.file.path))
    Dir.glob("#{@output_path}#{@filename}_*.txt").each do |filepath|
      article_page = ArticlePage.find_or_initialize_by(article_id: @article.id, page: page_number(filepath))
      next if !article_page.value.blank? && !override
      article_page.value = page_text(filepath)
      article_page.save
    end
  end

  def page_text(filepath)
    File.read(filepath)
  end

  def page_number(filepath)
    file = filepath.split('/').last
    file.split('_').last.split('.').first.to_i
  end
end
