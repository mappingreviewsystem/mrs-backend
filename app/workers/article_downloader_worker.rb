class ArticleDownloaderWorker
  include Sidekiq::Worker
  sidekiq_options queue: 'download', retry: 3

  def perform(article_id, override = false)
    return true if article_id.blank?

    article = find_article(article_id)
    return true if article.blank? || (article.file.exists? && !override)

    filename = download_article(article)
    return true if filename.blank?

    file = File.open(filename)
    article.file = file
    article.save
  end

  private

  def find_article(article_id)
    article = Article.find article_id
    logger.warn "Article \##{article_id} not found!" if article.nil?
    logger.warn "Article \##{article_id} need a Database to be downloaded!" if article.article_database.nil?

    article
  end

  def download_article(article)
    database = "ArticleDownloader::#{article.article_database.title}Database".constantize
    filename = database.download_article(article)
    logger.warn "Article \##{article.id} cannot be downloaded!" if filename.blank?

    filename
  end
end
