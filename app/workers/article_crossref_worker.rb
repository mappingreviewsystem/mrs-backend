class ArticleCrossrefWorker
  include Sidekiq::Worker
  sidekiq_options queue: 'crossref', retry: 3

  def perform(article_id, references = false, force = false)
    ArticleCrossrefService.crossref_sync(article_id, references, force)
  end
end
