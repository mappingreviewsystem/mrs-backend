class ArticleImporterWorker
  include Sidekiq::Worker

  def expiration
    @expiration ||= 60 * 30 # 30 minutes
  end

  def perform(article_database_id, download = false)
    return true if article_database_id.blank?
    find(article_database_id)

    ArticleImporter.import_sync(article_database: @article_database, download: download)
  end

  private

  def find(article_database_id)
    @article_database = ArticleDatabase.find article_database_id
    raise "\##{article_database_id} not found!" if @article_database.nil?
  end
end
