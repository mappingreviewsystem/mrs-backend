class ResearchQuestionSerializer < ActiveModel::Serializer
  attributes :id, :title, :description, :value

  belongs_to :research, embed: :id, embed_in_root: true
  has_many :extraction_questions, embed: :ids, embed_in_root: true
end
