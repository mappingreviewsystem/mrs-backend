class ContentTypeSerializer < ActiveModel::Serializer
  attributes :id, :code
end
