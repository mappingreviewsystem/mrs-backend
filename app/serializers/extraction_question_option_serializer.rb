class ExtractionQuestionOptionSerializer < ActiveModel::Serializer
  attributes :id, :title

  belongs_to :extraction_question, embed: :id, embed_in_root: true
  has_many :article_extraction_questions, embed: :ids, embed_in_root: true
  has_many :articles, embed: :ids, embed_in_root: true
end
