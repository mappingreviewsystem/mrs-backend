class ArticleExclusionCriteriumSerializer < ActiveModel::Serializer
  attributes :id

  belongs_to :article, embed: :id, embed_in_root: true
  belongs_to :exclusion_criterium, embed: :id, embed_in_root: true
end
