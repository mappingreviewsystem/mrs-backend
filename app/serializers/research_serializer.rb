class ResearchSerializer < ActiveModel::Serializer
  attributes :id, :title, :objective, :articles_count

  belongs_to :user, embed: :id, embed_in_root: true
  belongs_to :group, embed: :id, embed_in_root: true
  # has_one :current_user, embed: :id, embed_in_root: true

  has_many :keywords, embed: :ids, embed_in_root: true
  has_many :articles, embed: :ids, embed_in_root: true
  has_many :selected_articles, embed: :ids, embed_in_root: true
  has_many :article_references, embed: :ids, embed_in_root: true
  has_many :tags, embed: :ids, embed_in_root: true
  has_many :notes, embed: :ids, embed_in_root: true
  has_many :article_databases, embed: :ids, embed_in_root: true
  has_many :article_databases, embed: :ids, embed_in_root: true
  has_many :research_questions, embed: :ids, embed_in_root: true
  has_many :extraction_questions, embed: :ids, embed_in_root: true
  has_many :inclusion_criteria, embed: :ids, embed_in_root: true
  has_many :exclusion_criteria, embed: :ids, embed_in_root: true

  def authors_list
    object.selected_articles.joins(:authors).group('authors.id').count
  end

  def publications_list
    object.selected_articles.joins(:publication).group('publications.id').count
  end
end
