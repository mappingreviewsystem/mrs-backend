class AuthorSerializer < ActiveModel::Serializer
  attributes :id, :name, :given, :family

  has_many :articles, embed: :ids, embed_in_root: true
  has_many :affiliations, embed: :ids, embed_in_root: true
  belongs_to :research, embed: :id, embed_in_root: true
end
