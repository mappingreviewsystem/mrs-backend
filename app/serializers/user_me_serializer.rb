class UserMeSerializer < ActiveModel::Serializer
  attributes :id, :name, :email

  has_many :researches, embed: :ids, embed_in_root: true
  has_many :group_researches, embed: :ids, embed_in_root: true
  has_many :groups, embed: :ids, embed_in_root: true
  has_many :group_memberships, embed: :ids, embed_in_root: true
  belongs_to :current_research, embed: :id, embed_in_root: true
end
