class ResearchStatisticsSerializer < ActiveModel::Serializer
  attributes :id, :author_articles, :year_articles, :affiliation_authors, :publication_articles, :country_affiliations, :article_database_articles

  def author_articles
    hash = {}
    object.authors.joins(:articles).where(articles: { status: ['extracted', 'selected'] }).each { |author| hash[author.id] = author.articles.where(status: ['extracted', 'selected']).pluck(:id) }
    hash
  end

  def year_articles
    hash = {}
    object.articles.where(status: ['extracted', 'selected']).each do |article|
      hash[article.publication_year] ||= []
      hash[article.publication_year] << article.id
    end
    hash
  end

  def affiliation_authors
    hash = {}
    object.affiliations.joins(:authors, :articles).where(authors: { articles: { status: ['extracted', 'selected'] } }).map { |affiliation| hash[affiliation.id] = affiliation.authors.pluck(:id) }
    hash
  end

  def publication_articles
    hash = {}
    object.publications.joins(:articles).where(articles: { status: ['extracted', 'selected'] }).map { |publication| hash[publication.id] = publication.articles.where(status: ['extracted', 'selected']).pluck(:id) }
    hash
  end

  def country_affiliations
    hash = {}
    object.affiliations.joins(:authors, :articles).where(authors: { articles: { status: ['extracted', 'selected'] } }).uniq.map do |affiliation|
      hash[affiliation.location] ||= []
      hash[affiliation.location] << affiliation.id
    end
    hash
  end

  def article_database_articles
    hash = {}
    object.article_databases.map do |article_database|
      hash[article_database.id] = article_database.articles.where(research_id: object.id, status: ['extracted', 'selected']).pluck(:id)
    end
    hash[0] = Article.where(research_id: object.id, article_database_id: nil, status: ['extracted', 'selected']).pluck(:id)
    hash
  end
end
