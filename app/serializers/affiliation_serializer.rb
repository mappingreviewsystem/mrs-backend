class AffiliationSerializer < ActiveModel::Serializer
  attributes :id, :name, :location

  has_many :authors, embed: :ids, embed_in_root: true
  belongs_to :research, embed: :id, embed_in_root: true
end
