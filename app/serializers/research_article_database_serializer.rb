class ResearchArticleDatabaseSerializer < ActiveModel::Serializer
  attributes :id, :query, :status, :exported_search, :downloaded, :missing_lines, :created_at, :updated_at, :exported_search_updated_at

  belongs_to :research, embed: :id, embed_in_root: true
  belongs_to :article_database, embed: :id, embed_in_root: true
end
