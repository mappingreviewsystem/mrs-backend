class NodeSerializer < ActiveModel::Serializer
  # expecting an article
  type 'node' # jsonapi uses singular

  attributes :id, :title, :article, :label, :shape, :color, :value, :mass, :level, :status, :hidden, :relevant

  def article
    object
  end

  def relevant
    object.relevant?
  end

  # TODO: find a cleaner way to do it, try to store in db to calculate once
  def title
    "
    <b>#{object.title}</b><br>
    #{"(<i>#{object.publication_year}</i>) - " unless object.publication_year.nil?}#{object.authors.join('; ') unless object.authors.empty?}<br>
    <br>
    <a data-href=\"/articles/#{object.id}\" class=\"ajax-modal\">ver artigo</a>
    "
  end

  def label
    "#{object.title.truncate(30, separator: ' ')}\n#{object.first_author}\n#{object.referenced_by.count} / #{Article.maximum('referenced_by_count')}"
  end

  def shape
    case object.status
    when 'extracted' || 'selected'
      'box'
    when 'unseen'
      'text'
    else
      'ellipse'
    end
  end

  def color
    case object.status
    when 'extracted' || 'selected'
      '#80CBC4'
    when 'unseen'
      '#FFAB91'
    when 'excluded'
      '#E57373'
    else
      if object.referenced?
        '#2196F3'
      else
        '#e6e2d3'
      end
    end
  end

  def value
    value = case object.status
    when 'extracted' || 'selected' then 20 + (object.referenced_by.count * 5)
            # when 'not_included' then 5
            else 10 + (object.referenced_by.count * 5)
            end

    value
  end
  def mass
    1
  end

  def level
    max = Article.maximum(:referenced_by_count)
    level = max - object.referenced_by.count
    level -= 1 if object.chosen?
    level
  end

  def hidden
    !(object.chosen? || (object.referenced_by.count > 1))
  end
end
