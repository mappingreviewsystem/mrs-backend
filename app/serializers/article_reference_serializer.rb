class ArticleReferenceSerializer < ActiveModel::Serializer
  type 'article-reference'

  attributes :id, :status

  belongs_to :source, embed: :id, embed_in_root: true
  belongs_to :destination, embed: :id, embed_in_root: true
end
