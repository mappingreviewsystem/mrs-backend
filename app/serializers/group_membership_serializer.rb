class GroupMembershipSerializer < ActiveModel::Serializer
  attributes :id, :owner

  belongs_to :user, embed: :id, embed_in_root: true
  belongs_to :group, embed: :id, embed_in_root: true
end
