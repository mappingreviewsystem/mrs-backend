class InclusionCriteriumSerializer < ActiveModel::Serializer
  attributes :id, :title

  belongs_to :research, embed: :id, embed_in_root: true
end
