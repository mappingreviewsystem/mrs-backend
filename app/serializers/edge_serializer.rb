class EdgeSerializer < ActiveModel::Serializer
  type 'article-reference' # jsonapi dasherize and singularize
  # expecting an article_reference
  # attributes :source, :destination

  has_one :source, type: 'article', embed: :id, embed_in_root: true
  has_one :destination, type: 'article', embed: :id, embed_in_root: true
end
