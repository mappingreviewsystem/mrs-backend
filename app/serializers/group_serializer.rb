class GroupSerializer < ActiveModel::Serializer
  attributes :id, :name, :description

  has_many :researches, embed: :ids, embed_in_root: true
  has_many :users, embed: :ids, embed_in_root: true
  has_many :group_memberships, embed: :ids, embed_in_root: true
  has_many :owners, embed: :ids, embed_in_root: true
end
