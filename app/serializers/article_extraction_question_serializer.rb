class ArticleExtractionQuestionSerializer < ActiveModel::Serializer
  attributes :id, :value

  belongs_to :article, embed: :id, embed_in_root: true
  belongs_to :extraction_question, embed: :id, embed_in_root: true
  belongs_to :extraction_question_option, embed: :id, embed_in_root: true

  has_many :extraction_question_options, embed: :ids, embed_in_root: true
end
