class NoteSerializer < ActiveModel::Serializer
  attributes :id, :title, :value, :color, :priority

  belongs_to :article, embed: :id, embed_in_root: true
  belongs_to :research, embed: :id, embed_in_root: true
  has_many :tags, embed: :ids, embed_in_root: true
end
