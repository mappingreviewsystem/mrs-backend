class PublicationSerializer < ActiveModel::Serializer
  attributes :id, :title, :issn, :isbn, :location, :publication_type

  has_many :articles, embed: :ids, embed_in_root: true
  belongs_to :publisher, embed: :id, embed_in_root: true
  belongs_to :research, embed: :id, embed_in_root: true
end
