class ArticleSearchSerializer < ActiveModel::Serializer
  type 'articles'

  attributes :id, :title, :publication_year, :status, :referenced_by_count, :references_count, :url, :first_author, :file

  has_many :tags, embed: :ids, embed_in_root: true
  has_many :authors, embed: :ids, embed_in_root: true
  has_many :references, embed: :ids, embed_in_root: true
  has_many :referenced_by, embed: :ids, embed_in_root: true
  belongs_to :publisher, embed: :id, embed_in_root: true
  belongs_to :content_type, embed: :id, embed_in_root: true
end
