class TagSerializer < ActiveModel::Serializer
  attributes :id, :label, :color

  belongs_to :research, embed: :id, embed_in_root: true
  has_many :articles, embed: :ids, embed_in_root: true
  has_many :notes, embed: :ids, embed_in_root: true
end
