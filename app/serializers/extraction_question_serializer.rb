class ExtractionQuestionSerializer < ActiveModel::Serializer
  attributes :id, :key, :value, :field_type, :range_min, :range_max, :range_increment

  belongs_to :research, embed: :id, embed_in_root: true
  has_many :research_questions, embed: :ids, embed_in_root: true
  has_many :articles, embed: :ids, embed_in_root: true
  has_many :article_extraction_questions, embed: :ids, embed_in_root: true
  has_many :extraction_question_options, embed: :ids, embed_in_root: true
end
