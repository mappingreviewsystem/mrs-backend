class ArticleInclusionCriteriumSerializer < ActiveModel::Serializer
  attributes :id

  belongs_to :article, embed: :id, embed_in_root: true
  belongs_to :inclusion_criterium, embed: :id, embed_in_root: true
end
