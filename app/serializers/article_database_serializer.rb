class ArticleDatabaseSerializer < ActiveModel::Serializer
  attributes :id, :title, :url, :query, :status, :exported_search, :downloaded, :missing_lines, :created_at, :updated_at, :exported_search_updated_at

  has_many :articles, embed: :ids, embed_in_root: true
  belongs_to :research, embed: :id, embed_in_root: true
end
