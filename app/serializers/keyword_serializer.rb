class KeywordSerializer < ActiveModel::Serializer
  attributes :id, :title

  has_many :articles, embed: :ids, embed_in_root: true
  has_many :researches, embed: :ids, embed_in_root: true
end
