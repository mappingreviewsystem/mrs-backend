class ArticleSerializer < ActiveModel::Serializer
  type 'article'

  attributes :id, :title, :url, :publication_year, :doi, :volume, :issue, :page_start, :page_end, :isbn, :issn, :status, :data, :created_at, :updated_at, :file, :referenced_by_count, :references_count, :first_author

  belongs_to :article_database, embed: :id, embed_in_root: true
  belongs_to :research, embed: :id, embed_in_root: true
  belongs_to :content_type, embed: :id, embed_in_root: true
  has_many :authors, embed: :ids, embed_in_root: true
  has_many :keywords, embed: :ids, embed_in_root: true
  has_many :tags, embed: :ids, embed_in_root: true
  has_many :notes, embed: :ids, embed_in_root: true
  has_many :article_inclusion_criteria, embed: :ids, embed_in_root: true
  has_many :article_exclusion_criteria, embed: :ids, embed_in_root: true
  has_many :article_extraction_questions, embed: :ids, embed_in_root: true
  has_many :references, embed: :ids, embed_in_root: true
  has_many :referenced_by, embed: :ids, embed_in_root: true
  has_one :publication, embed: :id, embed_in_root: true
end
