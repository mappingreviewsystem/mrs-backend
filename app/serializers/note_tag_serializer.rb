class NoteTagSerializer < ActiveModel::Serializer
  attributes :id
  belongs_to :note, embed: :id, embed_in_root: true
  belongs_to :tag, embed: :id, embed_in_root: true
end
