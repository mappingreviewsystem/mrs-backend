class FunderSerializer < ActiveModel::Serializer
  attributes :id, :name, :url, :location, :crossref_id, :doi
end
