MrsSchema = GraphQL::Schema.define do
  mutation(Types::MutationType)
  query(Types::QueryType)

  max_depth 10

  resolve_type ->(type, object, ctx) do
    # binding.pry
    object.class.graph_ql_type
  end
end
