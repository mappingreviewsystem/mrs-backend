# Types::ReferenceGraphType = GraphQL::UnionType.define do
#   name 'ReferenceGraph'
#   description 'Graph of research\'s articles references'
#   possible_types [Types::ArticleType, Types::ArticleReferenceType]
# end

Types::ReferenceGraphType = GraphQL::ObjectType.define do
  name 'ReferenceGraph'
  description 'Graph of research\'s articles references'

  field :articles, types[Types::ArticleType]
  field :articleReferences, types[Types::ArticleReferenceType]
end
