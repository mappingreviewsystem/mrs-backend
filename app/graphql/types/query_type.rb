Types::QueryType = GraphQL::ObjectType.define do
  name 'Query'
  # Add root-level fields here.
  # They will be entry points for queries on your schema.

  field :referenceGraphs, types[Types::ReferenceGraphType] do
    description "Returns a graph from research's articles references"
    resolve ->(obj, args, ctx) {
      @research = Research.find 1
      # binding.pry
      @nodes = @research.articles.includes(:authors).where('(articles.status = ? OR articles.status = ?) OR articles.referenced_by_count > 1', Article.statuses['extracted'], Article.statuses['selected'])

      node_ids = @nodes.pluck(:id).uniq
      @edges = ArticleReference.where('source_id IN (?) AND destination_id IN (?)', node_ids, node_ids)

      # @nodes.to_a.concat(@edges.to_a)
      binding.pry
      return Types::ReferenceGraphType.new(articles: @nodes, articleReferences: @edges)
    }
  end
end
