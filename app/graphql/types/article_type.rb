Types::ArticleType = GraphQL::ObjectType.define do
  name 'Article'
  description 'An article'
  interfaces [Types::ActiveRecordTimestampType]


  field :id, !types.ID
  field :title, !types.String
  field :contentType, !types.String, property: :content_type
  field :url, !types.String
  field :publicationYear, !types.String, property: :publication_year
  field :doi, !types.String
  field :volume, !types.String
  field :issue, !types.String
  field :pageStart, !types.String, property: :page_start
  field :pageEnd, !types.String, property: :page_end
  field :isbn, !types.String
  field :issn, !types.String
  field :status, !types.String
  field :data, !types.String
  field :articleDatabaseId, !types.String, property: :article_database_id
  field :file, !types.String
  field :researchId, !types.String, property: :research_id
  field :referencedByCount, !types.String, property: :referenced_by_count
  field :referencesCount, !types.String, property: :references_count
end
