Types::ArticleReferenceType = GraphQL::ObjectType.define do
  name 'ArticleReference'

  field :id, !types.ID
  field :status, !types.String
  field :created_at, !types.String
  field :updated_at, !types.String
  field :source_id, !types.String
  field :destination_id, !types.String
end
